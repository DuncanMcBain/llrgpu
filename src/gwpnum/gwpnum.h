#ifndef _GWPNUMI_
#define _GWPNUMI_

/* Include Files */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#if defined (__linux__) || defined (__FreeBSD__) || defined (__APPLE__)
#include <sys/time.h>
#else
#include <time.h>
#endif
#include <sys/timeb.h>


/* compiler options */

#ifdef _WIN32
#pragma warning( disable : 4127 4706 ) /* disable conditional is constant warning */
#endif

#define TIMER_NL	0x1
#define TIMER_CLR	0x2
#define TIMER_OPT_CLR	0x4
#define NBTIMERS 20

// Global flags and data defined in gwpnumi.c file

extern	int FFTLEN;
extern	int MULBYCONST;
extern	int zp;
extern	int generic;
extern	int inc;
extern	int debug;
extern	int verbose;
extern	int E_CHK;
extern	double MAXERR;
extern	double gwptimers[];

typedef double* gwpnum;

/* function prototypes */

#define gwpfree(ptr) if ((void*)ptr != NULL) {free (ptr); ptr = NULL;}

void gwpclear_timers (void);

/**
 * Added by Duncan McBain, removes the timers given in the array,
 * with length in second argument.
 **/
void gwpclear_array_timers (
	int *,
	int
);

void gwpclear_timer (
	int
);

void gwpstart_timer ( 
	int
); 

void gwpend_timer ( 
	int
);
 
void gwpdivide_timer (
	int,
	int
);

double gwptimer_value (
	int
);

void gwpprint_timer (
	int,
	int
);

void gwpwrite_timer (		// JP 23/11/07
	char*,
	int, 
	int
	);

void gwpclearline (int
);

/* ------------ gwpnum - specific routines ------------------- */

void gwpsetoutputs (	// Get the pointers to user output functions
	void(*screenf)(char *), 
	void(*bothf)(char *)
);

int
gwpsetup(			// Initialize the gwpnum system
	double,			// The multiplier
	unsigned long,	// The base (generic mode forced if not two)
	unsigned long,	// The exponent
	signed long,	// c, in k*b^n+c (force generic reduction if neither +1 nor -1)
	giant			// modulus
);

int gwpsetup_general_mod_giant (
	giant			// The modulus of the modular reduction
);

void gwpset_larger_fftlen_count(
	int
);

void gwpfft_description (
	char *
);

gwpnum gwpalloc(			// Memory allocation
);

void itogwp(				// Integer to gwpnum
	int,
	gwpnum
);

void gwpaddsmall(			// Add a small value to a gwpnum
	gwpnum,
	int
);

void gwpsetaddin(			// Set the addin constant before normalizing
	long
);

void gwpsetaddinatpowerofb (
	long,
	unsigned long
);

void gwpsetmaxmulbyconst(	// Set the maximum of the multiplicative constant
	long
);

void gwpsetmulbyconst(		// Set the multiplicative constant before normalizing
	long
);

int gwptogiant (
	gwpnum,
	giant
);

void gianttogwp (
	giant,
	gwpnum
);

// User side large integers arithmetic operations

void gwpcopy (
	gwpnum,
	gwpnum
);

void					// Square a large integer
gwpsquare(
	gwpnum
);

void					// Square a large integer
gwpsquare_gpu(
	gwpnum
);

void					// Multiply two large integers
gwpmul(
	gwpnum,
	gwpnum
);

void gwpaddquick (
	gwpnum,
	gwpnum
);

void gwpsubquick (
	gwpnum,
	gwpnum
);

void gwpadd (
	gwpnum s,
	gwpnum d
);

void gwpsub (
	gwpnum s,
	gwpnum d
);

void gwpadd3 (
	gwpnum s1,
	gwpnum s2,
	gwpnum d
);

void gwpsub3 (
	gwpnum s1,
	gwpnum s2,
	gwpnum d
);

/* Square a number using a slower method that will have reduced */
/* round-off error on non-random input data.*/

void gwpsquare_carefully (
	gwpnum
);

/* Multiply numbers using a slower method that will have reduced */
/* round-off error on non-random input data.*/

void gwpmul_carefully (
	gwpnum,
	gwpnum
);

#define gwpsetnormroutine(z,e,c) {E_CHK=(e);MULBYCONST=(c);}

int					// Test is the large integer is zero
gwpiszero(
	gwpnum
);

int	gwpequal (		// Test two gwpnums for equality
	gwpnum, 
	gwpnum
);

/********************* Internal functions ***************************/

double				// Normalize a large integer
gwpnormalize(
	gwpnum
);

void gwpcopyzero (
	gwpnum,
	gwpnum,
	unsigned long
);

void gwpsetzero (
	gwpnum,
	unsigned long
);

void	gwpdone (
void
);					// Free all the memory used by this code.

#endif
