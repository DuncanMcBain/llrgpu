#ifndef CUDA_GWPNUM_H
#define CUDA_GWPNUM_H

#include <cufft.h>

extern cufftDoubleReal * devDataIn, * twoToPhi, * twoToMinusPhi;

int initcufft(int n);
void fwdfftcuda();
void bwdfftcuda();
void cuda_cleanup();
void _square_complex_gpu(cufftDoubleComplex *, int);
void convolution_in(cufftDoubleReal *, cufftDoubleReal *, double *, int);
void convolution_out(cufftDoubleReal *, cufftDoubleReal *, double *, int);
double inormalize_cuda(cufftDoubleReal *, double, int, double, int, double, int);
void copyNumberOn(cufftDoubleReal *, int);
void copyNumberOff(cufftDoubleReal *, int);

#endif
