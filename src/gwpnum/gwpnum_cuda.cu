/* CUDA functions for gwpnum.c
 * Duncan McBain, 28.6.13
 */

#include <stdio.h>
#include <cuda_runtime.h>
#include <cufft.h>
#include <cuda_profiler_api.h>
extern "C" {
#include "cuda_gwpnum.h"
}

typedef double fftw_complex[2];

extern cufftHandle dpfwd, dpbwd;
extern cufftDoubleComplex * devDataOut;
extern double * xin, * limitbv, * invlimit;
extern fftw_complex * cxout;
extern int FFTLEN;

double * limitbv_dev, * invlimitbv_dev, * carry_array;
cudaError_t retval;
cufftResult curetval;

__device__ int carries_left;
__device__ double maxerr;

__global__ void addConstantToArray(cufftDoubleReal *, double, int);
__global__ void square_elem(cufftDoubleComplex *, int);
__global__ void convolution_elem(cufftDoubleReal *, cufftDoubleReal *, int);
__global__ void normalize_elem(cufftDoubleReal *, double *, double *,
							   double *, int, double);
__global__ void normalize2(cufftDoubleReal *, double *, int);
__global__ void normalize3(cufftDoubleReal *, double *, double *, double *, int);
__global__ void propagate_carry(cufftDoubleReal *, cufftDoubleReal *, double *,
								double *, int, double, double, int);

extern "C"
int initcufft(int n)
{
	cudaProfilerStart();
	retval = cudaMalloc(&devDataIn, n * sizeof(cufftDoubleReal));
	if(retval != cudaSuccess)
		fprintf(stderr,"in cudamalloc failure: %s\n",cudaGetErrorString(retval));
	retval = cudaMalloc(&devDataOut, (n/2 + 1) * sizeof(cufftDoubleComplex));
	if(retval != cudaSuccess)
		fprintf(stderr,"out cudamalloc failure: %d\n",retval);
	retval = cudaMalloc(&twoToPhi, FFTLEN * sizeof(cufftDoubleReal));
	if(retval != cudaSuccess)
		fprintf(stderr,"2tophi cudamalloc failure: %d\n",retval);
	retval = cudaMalloc(&twoToMinusPhi, FFTLEN * sizeof(cufftDoubleReal));
	if(retval != cudaSuccess)
		fprintf(stderr,"2two-phi cudamalloc failure: %d\n",retval);
	curetval = cufftPlan1d(&dpfwd, n, CUFFT_D2Z, 1);
	if(curetval != CUFFT_SUCCESS)
		fprintf(stderr,"fwd plan failure: %d\n",curetval);
	curetval = cufftPlan1d(&dpbwd, n, CUFFT_Z2D, 1);
	if(curetval != CUFFT_SUCCESS)
		fprintf(stderr,"bwd plan failure: %d\n",curetval);
	retval = cudaMalloc(&limitbv_dev, n * sizeof(double));
	if(retval != cudaSuccess)
		fprintf(stderr,"limitbv cudamalloc failure: %d\n",retval);
	retval = cudaMalloc(&invlimitbv_dev, n * sizeof(double));
	if(retval != cudaSuccess)
		fprintf(stderr,"invlimitbv cudamalloc failure: %d\n",retval);
	retval = cudaMalloc(&carry_array, (n + 1) * sizeof(double));
	if(retval != cudaSuccess)
		fprintf(stderr,"carry cudamalloc failure: %d\n",retval);
	retval = cudaMemcpy(limitbv_dev, limitbv, n * sizeof(double), cudaMemcpyHostToDevice);
	retval = cudaMemcpy(invlimitbv_dev, invlimit, n * sizeof(double), cudaMemcpyHostToDevice);

	return 0;
}

// Copy the number to be tested to GPU memory
extern "C"
void copyNumberOn(cufftDoubleReal * x, int len)
{
	retval = cudaMemcpy(devDataIn, x, sizeof(double) * len, cudaMemcpyHostToDevice);
	if(retval != cudaSuccess)
		fprintf(stderr,"dev data copy in failure: %d\n",retval);
}

// Copy the number that has been tested to host memory
extern "C"
void copyNumberOff(cufftDoubleReal * x, int len)
{
	retval = cudaMemcpy(x, devDataIn, sizeof(double) * len, cudaMemcpyDeviceToHost);
	if(retval != cudaSuccess)
		fprintf(stderr,"dev data copy out failure: %d\n",retval);
}

extern "C"
void fwdfftcuda()
{
	curetval = cufftExecD2Z(dpfwd, devDataIn, devDataOut);
	if(curetval != CUFFT_SUCCESS)
		fprintf(stderr,"fwd exec failure: %d\n",curetval);
}

extern "C"
void bwdfftcuda()
{
	curetval = cufftExecZ2D(dpbwd, devDataOut, devDataIn);
	if(curetval != CUFFT_SUCCESS)
		fprintf(stderr,"bwd exec failure: %d\n",curetval);
}

extern "C"
void _square_complex_gpu(cufftDoubleComplex * x, int len)
{
	int blocksPerGrid;
	int threadsPerBlock;
	if(!(len % 256))
		blocksPerGrid = len / 256;
	else
		blocksPerGrid = (len / 256) + 1;

	threadsPerBlock = 256;
		
	square_elem<<<blocksPerGrid, threadsPerBlock>>>(x, len);
	retval = cudaGetLastError();
	if(retval != cudaSuccess)
		fprintf(stderr,"kernel launch failure: %d\n",retval);
	// Apparently ThreadSynchronize is deprecated!
	cudaDeviceSynchronize();
}

// Next two functions basically the same - make them one again
extern "C"
void convolution_in(cufftDoubleReal * x, cufftDoubleReal * y, double * z, int len)
{
	int blocksPerGrid;
	int threadsPerBlock;
	if(!(len % 256))
		blocksPerGrid = len / 256;
	else
		blocksPerGrid = (len / 256) + 1;

	threadsPerBlock = 256;
		
	convolution_elem<<<blocksPerGrid, threadsPerBlock>>>(x, y, FFTLEN);
	retval = cudaGetLastError();
	if(retval != cudaSuccess)
		fprintf(stderr,"cin kernel launch failure: %s\n",cudaGetErrorString(retval));
	cudaDeviceSynchronize();
}

extern "C"
void convolution_out(cufftDoubleReal * x, cufftDoubleReal * y, double * z, int len)
{
	int blocksPerGrid;
	int threadsPerBlock;
	if(!(len % 256))
		blocksPerGrid = len / 256;
	else
		blocksPerGrid = (len / 256) + 1;

	threadsPerBlock = 256;
		
	convolution_elem<<<blocksPerGrid, threadsPerBlock>>>(x, y, FFTLEN);
	retval = cudaGetLastError();
	if(retval != cudaSuccess)
		fprintf(stderr,"cout kernel launch failure: %s\n",cudaGetErrorString(retval));
	cudaDeviceSynchronize();
}

extern "C"
double inormalize_cuda(cufftDoubleReal * x, double bigvalue,
					 int wrapindex, double wrapfactor,
					 int addinindex, double addinvalue, int len)
{
	int blocksPerGrid;
	int threadsPerBlock = 256;
	double error = 0;
	if(!(len % threadsPerBlock))
		blocksPerGrid = len / threadsPerBlock;
	else
		blocksPerGrid = (len / threadsPerBlock) + 1;

	// Set memory to zero, as carries should be, also prepare the addinvalue in the
	// correct position
	int byteval = 0;
	addConstantToArray<<<1,1>>>(x, addinvalue, addinindex);
	cudaDeviceSynchronize();
	retval = cudaGetLastError();
	if(retval != cudaSuccess)
		fprintf(stderr,"addinvalues launch failure: %s\n", cudaGetErrorString(retval));
	retval = cudaMemset(carry_array, byteval, (len + 1) * sizeof(double));
	if(retval != cudaSuccess)
		fprintf(stderr,"carry cudamemset failure: %s\n", cudaGetErrorString(retval));

	// First normalisation, rounds numbers and calculates carries
	normalize_elem<<<blocksPerGrid, threadsPerBlock>>>(x, carry_array, limitbv_dev, invlimitbv_dev,
													   FFTLEN, bigvalue);
	cudaDeviceSynchronize();
	retval = cudaGetLastError();
	if(retval != cudaSuccess)
		fprintf(stderr,"normalize launch failure: %s\n", cudaGetErrorString(retval));

	int carries_reconciled = 0, carries_remaining = 0;
	const double zero = 0.0;
	while(!carries_reconciled)
	{
		// Adds carries to the relevant elements, separate kernel to ensure data safety
		normalize2<<<blocksPerGrid,threadsPerBlock>>>(x, carry_array, FFTLEN);
		cudaDeviceSynchronize();
		retval = cudaGetLastError();
		if(retval != cudaSuccess)
			fprintf(stderr,"normalize2 launch failure: %s\n", cudaGetErrorString(retval));

		// Setting all but the last carry element back to zero
		retval = cudaMemset(carry_array, byteval, len * sizeof(double));
		if(retval != cudaSuccess)
			fprintf(stderr,"carry cudamemset failure: %s\n", cudaGetErrorString(retval));

		normalize3<<<blocksPerGrid,threadsPerBlock>>>(x, carry_array, limitbv_dev, invlimitbv_dev,
													  FFTLEN);
		cudaDeviceSynchronize();
		retval = cudaGetLastError();
		if(retval != cudaSuccess)
			fprintf(stderr,"normalize3 launch failure: %s\n", cudaGetErrorString(retval));

		retval = cudaMemcpyFromSymbol(&carries_remaining, carries_left, sizeof(int), 0, cudaMemcpyDeviceToHost);
		if(retval != cudaSuccess)
			fprintf(stderr,"carry data transfer launch failure: %s\n", cudaGetErrorString(retval));
		retval = cudaMemcpyToSymbol(carries_left, &zero, sizeof(int), 0, cudaMemcpyHostToDevice);
		if(retval != cudaSuccess)
			fprintf(stderr,"carry data transfer launch failure: %s\n", cudaGetErrorString(retval));

		fprintf(stderr,"%d carries remaining\n",carries_remaining);
		if(carries_remaining > 0)
			carries_reconciled = 0;
		else
			carries_reconciled = 1;
	}
	propagate_carry<<<1,1>>>(x, carry_array, limitbv_dev, invlimitbv_dev,
							 wrapindex, wrapfactor, bigvalue, FFTLEN);
	cudaDeviceSynchronize();
	retval = cudaGetLastError();
	if(retval != cudaSuccess)
		fprintf(stderr,"carry propagation launch failure: %s\n", cudaGetErrorString(retval));
	return error;
}

extern "C"
void cuda_cleanup()
{
	cudaProfilerStop();
	cufftDestroy(dpfwd);
	cufftDestroy(dpbwd);
	cudaFree(devDataIn);
	cudaFree(devDataOut);
}

// CUDA code from here on in

// This function squares a single element of the array, given by its thread
// index
__global__ void square_elem(cufftDoubleComplex *x, int length)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
	if(i >= length)
		return;
	double re = x[i].x * x[i].x - x[i].y * x[i].y;
	x[i].y = 2 * x[i].x * x[i].y;
	x[i].x = re;
}

__global__ void convolution_elem(cufftDoubleReal * x, cufftDoubleReal * y, int N)
{
	int i  = blockDim.x * blockIdx.x + threadIdx.x;
	if(i >= N)
		return;
	x[i] *= y[i];
}

// Calling this in a manner other than <<<1,1>>> will go poorly
__global__ void addConstantToArray(cufftDoubleReal * x, double addinvalue, int index)
{
	*(x + index) += addinvalue;
}

__global__ void normalize_elem(cufftDoubleReal * x, cufftDoubleReal * carries,
							   double * limitbv, double * invlimitbv, int len, double bv)
{
	double rounded, high_bits;
	int i  = blockDim.x * blockIdx.x + threadIdx.x;
	if(i >= len)
		return;
	rounded = ( *(x + i) + bv ) - bv;
	high_bits = (rounded + limitbv[i]) - limitbv[i];
	*(x + i) = rounded - high_bits;
	carries[i + 1] = high_bits * invlimitbv[i];
}

__global__ void normalize2(cufftDoubleReal * x, double * carries, int len)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
	if(i > len)
		return;
	*(x + i) += carries[i];
}

__global__ void normalize3(cufftDoubleReal * x, double * carries,
						   double * limitbv, double * invlimitbv, int len)
{
	double rounded, high_bits;
	int i = blockDim.x * blockIdx.x + threadIdx.x;
	if(i > len)
		return;
	rounded = *(x + i);
	high_bits = (rounded + limitbv[i]) - limitbv[i];
	*(x + i) = rounded - high_bits;
	carries[i + 1] += high_bits * invlimitbv[i];
	if(i != (len - 1))
		if(carries[i + 1] != 0)
			atomicAdd(&carries_left, 1);
}

// does not contain `plus' because for LLR numbers plus is always false
__global__ void propagate_carry(cufftDoubleReal * x, cufftDoubleReal * carries,
								double * limitbv, double * invlimit, int wrapindex,
								double wrapfactor, double bv, int len)
{
	int j;
	double rounded, high_bits, carry2, carry = carries[len];
	if (carry)
	{
		j = 0;
		if(wrapindex)
			carry2 = carry * wrapfactor;
		carry *= -1;
		while (carry || carry2)
		{
			// Pretty certain that this logic could be restructured to be much better,
			// but I really don't want to mess with it
			if (wrapindex && !carry)
				j = wrapindex;
			rounded = *(x + j) + carry;
			if (wrapindex && j == wrapindex)
			{
				rounded += carry2;
				carry2 = 0.0;
			}

			high_bits = (rounded + limitbv[j]) - limitbv[j];
			carry = high_bits * invlimit[j];
			*(x + j) = rounded - high_bits;

			if (++j == len)
			{
				j = 0;
				if (wrapindex)
					carry2 = carry * wrapfactor;
				carry *= -1;
			}
		}
	}
}

