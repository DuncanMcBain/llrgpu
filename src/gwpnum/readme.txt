Welcome to the gwpnum library.

	The aim of this library is to implement almost all the features of
	George Woltman's one in a portable code.
	(but for now, base != 2 numbers require generic modular reduction)
	It might be possible to built it on any system having a C/C++
	compiler, and a sufficiently powerful floating point feature.
	Indeed, when run on x86 systems, this code is much slower than
	Georges'one which is extremely optimized for almost all of them!
	This version has been adapted from the stand alone one, and requires
	to be linked with a library built with the FFTW package. The advantage
	is that a power of two FFT length is no more required.
	

-> how to use
	The code is initialized by calling gwpsetup (k, b, n, c, M)
	were M is the giant array containing the modulus of the
	large integer computations. M must be allocated and computed
	before calling gwpsetup.

	You may study gwpnum.h for a list of functions, which are mostly
	the same as those available in the gwnum library.  

-> how to compile / how to port

Windows:
	To make the libraries, your path must already be set to the proper
	C compiler (32-bit for now).  Then execute
		nmake /f compile all
	to build the gwpnumi.lib (standard) and gwpnumid.lib (debug) libraries.

Linux:
	Change to the gwpnum directory and execute
		make
	to build the gwpnumi.a library

