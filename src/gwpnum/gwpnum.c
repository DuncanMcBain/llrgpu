/**************************************************************
 *
 *	gwpnum.c
 *
 *  Modulo k*2^n+/-1 DWFFT multiplications and squarings prototype source code.
 *  Fully C and C++ written, no Assembler code.
 *  Nothing original here ; my goal is to have a code portable on any system having a C / C++ compiler,
 *  and, indeed a processor with a sufficiently powerful floating point unit!
 *  First : 14/09/2005 : uses George Woltman's 1/k IBDWT method and
 *  cyclic (c = -1), negacyclic (c = +1) real convolutions.
 *  Drawback in the negacyclic case : k must be small due to the 1/cos factor in the inverse DFFT.
 *  (this factor becomes large near the middle of the FFT array!)
 *  Updates:
 *  14/04/2008 : Full complex, half length convolution used when computing modulo k*2^n+1 .
 *  Nov. 2010 : zero-padded FFT implemented for k's up to 45 bits large.
 *  Dec. 2010 : generic modular reduction implemented for k's larger than 45 bits or for general form moduli.
 *  May. 2011 : This code must be linked with  Matteo Frigo and Steven G. Johnson's FFTW library.
 *  Thanks to this FFTW usage, a power of two FFT length is no more required.
 *  Dec. 2011 : As suggested by Steven G. Johnson and implemented by Iain Bethune, r2c interface is now used
 *  for both real-input and complex-input FFT's. Moreover, FFTW_PATIENT optimisation is chosen.
 *  Jean Penne  17/12/2011, E-mail : jpenne@free.fr
 *
 **************************************************************/

/* Include Files */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include "fftw3.h"
#if defined (__linux__) || defined (__FreeBSD__) || defined (__APPLE__)
#include <sys/time.h>
#define _timeb		timeb
#define _ftime		ftime
#else
#include <time.h>
#endif
#include "giants.h"
#include "gwdbldbl.h"
#include "gwpnum.h"
#include "../timings.h"
#include "cuda_gwpnum.h"
#include <cuda_runtime.h>
#include <cufft.h>


/* definitions */

#define BITS 16
#ifndef _MIPSBUILD_
#define MAXBITSPERDOUBLE (double)35
#define MAXKBITS 22
#else
#define MAXBITSPERDOUBLE (double)35
#define MAXKBITS 22
#endif

/* The maximum value k * mulbyconst that can be in a zero pad FFT.  Larger */
/* values must use generic modular reduction. */

#if defined (__linux__) || defined (__FreeBSD__) || defined (__APPLE__)
#define MAX_ZEROPAD_K	35184372088831.0	/* 45-bit k's seem to be OK. */
#else
#define MAX_ZEROPAD_K	68719476735.0		/* 36-bit k's seem to be OK. */
#endif

#define EB 10	// Extra bits of precision for generic reduction

#ifndef WIN32
#define LINE_FEED "\r"
#elif defined (_CONSOLE)
#define LINE_FEED "\r"
#else
#define LINE_FEED "\n"
#endif


/* global variables */

double gwptimers[2*NBTIMERS] = {0.0};		/* Up to NBTIMERS separate timers */

int		E_CHK;
int		CUMULATIVE_TIMING = 0;
double	MAXERR;
giant	gmodulus = NULL;
giant	grecip = NULL;
giant	gtmp = NULL;
gwpnum	GWP_RANDOM = NULL;
gwpnum	modulus = NULL;
gwpnum	recip = NULL;
gwpnum	gwptmp = NULL;
void	(*printfunction)(char*) = NULL;
void	(*screen_output)(char*) = NULL;
void	(*both_output)(char*) = NULL;

int		plus = 0, compl = 0, zp = 0, generic = 0, zcomplex = 0, verbose = 0;

double	*cn=NULL, *sn=NULL, *cnp=NULL, *snp=NULL, *two_to_phi=NULL, *two_to_minusphi=NULL,
		*w=NULL, *permutedx=NULL, *permutedy=NULL, *invlimit=NULL, *flimit=NULL,
		*hlimit=NULL, *limitbv=NULL;
double 	r, high, low, highinv, lowinv, last, lastinv, addinvalue, wrapfactor;
double	BIGVAL, kg, SMALLMULCONST = 1.0, MAXMULCONST = 1.0;
double	ttmp, avg_num_b_per_word;
double		*xin, *yin;
fftw_complex	*cxin, *cyin, *cxout, *cyout;
fftw_plan		fwpx, fwpy, bwpx, bwpy;
unsigned long 	ng, bit_length, zerowordslow, zerowordshigh; 
int 	 *ip=NULL, *permute=NULL, *fftbase=NULL, addinindex, wrapindex;
int		FFTLEN, debug = 0, MULBYCONST = 0, FFTINC = 0;

char	gwpbuf[256];

// Variables used for modular reduction in zero-padded mode

unsigned long temp = 0, rem, hwcount, lwcount, hwoffset, bits;
int		inc;
double	mult, invmult, shift, limit_high, limit_inverse_high, limit_high_bigval;
double	*scr, *scral;

// GPU FFT data and plans

cufftHandle dpfwd, dpbwd;
cufftDoubleReal * devDataIn, * twoToPhi, * twoToMinusPhi;
cufftDoubleComplex * devDataOut;

/**************************************************************
 *
 *	Functions
 *
 **************************************************************/

/* rint is not ANSI compatible, so we need a definition for 
 * WIN32 and other platforms with rint.
 */

double
RINT(double x)
{
	return floor(x + 0.5);
}

#define RINTP(x) ((x)-BIGVAL)+BIGVAL

// Allocation routine

gwpnum gwpalloc()
{
	return((gwpnum)malloc(FFTLEN*sizeof(double)));
}

// Utilities

void gwptrace (int n) {
	printfunction = (verbose)? both_output : screen_output;
	sprintf (gwpbuf, "OK until number %d\n", n);
	if (printfunction != NULL)
		(*printfunction)(gwpbuf);
}

void gwpclearline (int size) {
	char buf[256];
	int i;

	for (i=0; i<256; i++)
		buf[i] = '\0';
	for (i=0; i<size; i++)
		buf[i] = ' ';
	buf[size-1] = '\r';
#if !defined(WIN32) || defined(_CONSOLE)
	printf("%s", buf);
#endif
}


/* Routines used to time code chunks */

void gwpclear_timers () {
	int	i;
	for (i = 0; i < 2*NBTIMERS; i++) gwptimers[i] = 0.0;
}

/**
 * Added by Duncan McBain to stop gwpclear_timers() mulching my timers, too!
 **/
void gwpclear_array_timers (
	int * timers,
	int len)
{
	int i;
	for(i = 0; i < len; ++i)
		gwpclear_timer(timers[i]);
}

void gwpclear_timer (
	int	i)
{
	gwptimers[i] = 0.0;
}

void gwpstart_timer ( 
	int	i) 
{ 
	struct _timeb timeval; 
	if (i >= NBTIMERS)
		return;
	if (gwptimers[i+NBTIMERS] != 0.0)			// to avoid double start...
		return;
/*	if (HIGH_RES_TIMER) { 
		gwptimers[i] -= getHighResTimer (); 
	} else { */
		_ftime (&timeval); 
		gwptimers[i] -= (double) timeval.time * 1000.0 + timeval.millitm; 
//	} 
	gwptimers[i+NBTIMERS] = 1.0;				// to show that gwptimers[i] is already started
} 
 
void gwpend_timer ( 
	int	i) 
{ 
	struct _timeb timeval; 
	if (i >= NBTIMERS)
		return;
	if (gwptimers[i+NBTIMERS] == 0.0)			// to avoid double end...
		return;
/*	if (HIGH_RES_TIMER) { 
		gwptimers[i] += getHighResTimer (); 
	} else { */
		_ftime (&timeval); 
		gwptimers[i] += (double) timeval.time * 1000.0 + timeval.millitm; 
//	} 
	gwptimers[i+NBTIMERS] = 0.0;				// to show that gwptimers[i] is ended
} 
 
void gwpdivide_timer (
	int	i,
	int	j)
{
	gwptimers[i] = gwptimers[i] / j;
}

double gwptimer_value ( 
	int	i) 
{ 
/*	if (HIGH_RES_TIMER) 
		return (gwptimers[i] / getHighResTimerFrequency ()); 
	else */
		return (gwptimers[i] / 1000.0); 
} 
 
void gwpprint_timer (
	int	i,
	int	flags)
{ 
	char	buf[40]; 
	double	t; 
 
	t = gwptimer_value (i); 
	if (flags & TIMER_NL)
		if (t >= 1.0)  
			sprintf (buf, "%.3f sec."LINE_FEED"", t); 
		else 
			sprintf (buf, "%.3f ms."LINE_FEED"", t * 1000.0);
	else
		if (t >= 1.0)  
			sprintf (buf, "%.3f sec.", t); 
		else 
			sprintf (buf, "%.3f ms.", t * 1000.0);


	printfunction = screen_output;
	if (printfunction != NULL)
		(*printfunction)(buf);


	if (flags & TIMER_CLR) gwptimers[i] = 0.0; 
	if ((flags & TIMER_OPT_CLR) && !CUMULATIVE_TIMING) gwptimers[i] = 0.0; 
} 

void gwpwrite_timer (		// JP 23/11/07
	char* buf,
	int	i, 
	int	flags) 
{ 
	double	t; 
 
	t = gwptimer_value (i); 
	if (flags & TIMER_NL)
		if (t >= 1.0)  
			sprintf (buf, "%.3f sec.\n", t); 
		else 
			sprintf (buf, "%.3f ms.\n", t * 1000.0);
	else
		if (t >= 1.0)  
			sprintf (buf, "%.3f sec.", t); 
		else 
			sprintf (buf, "%.3f ms.", t * 1000.0);
 
 
	if (flags & TIMER_CLR) gwptimers[i] = 0.0; 
	if ((flags & TIMER_OPT_CLR) && !CUMULATIVE_TIMING) gwptimers[i] = 0.0; 
} 

void
print(
	double 	*x,
	int  	N
)
{
	int  	printed = 0;

	printfunction = (verbose)? both_output : screen_output;

	while (N >= 0)
	{
		if ((x[N]==0) && (!printed))
			goto decr;
		sprintf(gwpbuf, "%g  ",x[N]);
		if (printfunction != NULL)
			(*printfunction)(gwpbuf);
		printed=1;
decr:		N--;
	}
	sprintf(gwpbuf, "||\n");
	if (printfunction != NULL)
		(*printfunction)(gwpbuf);
}

// These functions do the relevant dyadic multiplications or squarings on Fourier transformed data

void
_mul_complex(
	fftw_complex 	*a,
	fftw_complex 	*b,
	int 			n
)
{
	register int 	k;
	register double Reb;

	for (k=0; k<n; k++) {
		Reb = a[k][0]*b[k][0]-a[k][1]*b[k][1];
		b[k][1] = a[k][1]*b[k][0]+a[k][0]*b[k][1];
		b[k][0] = Reb;
	}
}

void
_square_complex(
	fftw_complex *b,
	int n
)
{
	register int k;
	register double Reb;

	for (k=0; k<n; k++) {
		Reb = b[k][0]*b[k][0]-b[k][1]*b[k][1];
		b[k][1] = 2*b[k][1]*b[k][0];
		b[k][0] = Reb;
	}

}

// These function do the general multiplication or squaring of large integers, using DFFT

void fftwsquare_g (
	int size)
{
	register int j;
	register double ReX;

	if (compl) {	// Full complex, half size DWFFT
					// Multiply cxin by exp(i*j*pi/size) to prepare a right-angle convolution
		for (j=0; j<size/2; j++) {
			ReX = cnp[j]*cxin[j][0]-snp[j]*cxin[j][1];
			cxin[j][1] = cnp[j]*cxin[j][1]+snp[j]*cxin[j][0];
			cxin[j][0] = ReX;
		}

	}

	fftw_execute (fwpx);	// Execute the relevant forward DFFT

	if (compl)				// Compute the relevant Dyadic squaring
		_square_complex (cxout, size/2);
	else
		_square_complex(cxout, size/2+1);

	fftw_execute (bwpx);	// Execute the relevant backward DFFT

	if (compl) {			// Full complex, half size DWFFT
				// Multiply cxin by exp(-i*j*pi/size) to complete the right-angle convolution
		for (j=0; j<size/2; ++j)
		{
			ReX = cnp[j]*cxin[j][0]+snp[j]*cxin[j][1];
			cxin[j][1] = cnp[j]*cxin[j][1]-snp[j]*cxin[j][0];
			cxin[j][0] = ReX;
		}
	}
}

// not really fftwsquare_g so much as cufftsquare_g but that might change later
void fftwsquare_g_gpu(int size)
{
	register int j;
	register double ReX;

	gwpstart_timer(FFFT_TIMER);
	fwdfftcuda();
	gwpend_timer(FFFT_TIMER);

	gwpstart_timer(MULT_TIMER);
	_square_complex_gpu(devDataOut, size/2+1);
	gwpend_timer(MULT_TIMER);

	gwpstart_timer(BFFT_TIMER);
	bwdfftcuda();
	gwpend_timer(BFFT_TIMER);
}

void fftwmul_g (int size) {
	register int j;
	register double ReX, ReY;

	if (compl) {	// Full complex, half size DWFFT
			// Multiply cxin and cyin by exp(i*j*pi/size) to prepare a right-angle convolution
		for (j=0; j<size/2; j++) {
			ReX = cnp[j]*cxin[j][0]-snp[j]*cxin[j][1];
			cxin[j][1] = cnp[j]*cxin[j][1]+snp[j]*cxin[j][0];
			cxin[j][0] = ReX;
			ReY = cnp[j]*cyin[j][0]-snp[j]*cyin[j][1];
			cyin[j][1] = cnp[j]*cyin[j][1]+snp[j]*cyin[j][0];
			cyin[j][0] = ReY;
		}
	}

	fftw_execute (fwpx);	// Execute the two relevant forward DFFTs
	fftw_execute (fwpy);

	if (compl)				// Compute the relevant Dyadic product
		_mul_complex (cxout, cyout, size/2);
	else
		_mul_complex(cxout, cyout, size/2+1);

	fftw_execute (bwpy);	// Execute the relevant backward DFFT


	if (compl) {			// Full complex, half size DWFFT
				// Multiply cyin by exp(-i*j*pi/size) to complete the right-angle convolution
		for (j=0; j<size/2; ++j)
		{
			ReY = cnp[j]*cyin[j][0]+snp[j]*cyin[j][1];
			cyin[j][1] = cnp[j]*cyin[j][1]-snp[j]*cyin[j][0];
			cyin[j][0] = ReY;
		}
	}
}

//	Functions that do the normalization of the result of a multiplication or squaring

double
inormalize(									// Used for irrational bases DWFFT
	double 			*x,
	int 			N,
	int				error_log,
	int				noadd,
	int				nomul
)
{
	register int 	j;
	register double 	*px = x, xx, zz, bv = BIGVAL;
	register double  	carry = 0.0, carry2 = 0.0, err, maxerr = 0.0;

	if (!noadd)
		x[addinindex] += addinvalue;	// Add the optional small constant
	for (j=0; j<N; ++j)
	{
		if (MULBYCONST && !nomul)
			*px *= SMALLMULCONST;		// Optionaly multiply by a small constant
		xx = (*px + bv) - bv;			// Round to the nearest integer
		if (error_log ) {				// Compute the rounding error if required
			if (xx<0)
          			err = fabs(-xx + *px);  
     			else 
     				err = fabs(xx  - *px);
	 		if (err > maxerr) 
	 			maxerr = err;
     		}

		xx += carry;
		zz = (xx+limitbv[j])-limitbv[j];
		carry = zz*invlimit[j];			// Compute the carry on next word
		*(px++) = xx - zz;				// And the balanced remainder in current word

	}

	if (carry)
	{
		j = 0;
		px = x;
		if (wrapindex)
			carry2 = carry*wrapfactor;
		if (plus)
			carry = -carry;
		while (carry||carry2)
		{	if (wrapindex && !carry) {	// Skip already normalized words
				j = wrapindex;
				px = x + wrapindex;
			}
			xx = *px + carry;
			if (wrapindex && j==wrapindex) {
				xx += carry2;
				carry2 = 0.0;
			}

			zz = (xx+limitbv[j])-limitbv[j];
			carry = zz*invlimit[j];		// Compute the carry on next word
			*(px++) = xx - zz;			// And the balanced remainder in current word

			if (++j == N)
			{
				j = 0;
				px = x;
				if (wrapindex)
					carry2 = carry*wrapfactor;
				if (plus)
					carry = -carry;
			}
		}
	}
	return(maxerr);
}

double
rnormalize(									// Used for rational bases DWFFT
	double 			*x,
	int 				N,
	int				error_log,
	int				noadd,
	int				nomul
)
{
	register int 	j;
	register double 	*px = x, xx, zz, bv = BIGVAL, invlimit = limit_inverse_high;
	register double  	carry = 0.0, limitbv = (limit_high*BIGVAL)-BIGVAL, err, maxerr = 0.0;

	if (!noadd)
		x[addinindex] += addinvalue;	// Add the optional small constant
	for (j=0; j<N; ++j)
	{
		if (MULBYCONST && !nomul)
			*px *= SMALLMULCONST;		// Optionaly multiply by a small constant
		xx = (*px + bv) - bv;			// Round to the nearest integer
		if (error_log ) {				// Compute the rounding error if required
			if (xx<0)
          			err = fabs(-xx + *px);  
     			else 
     				err = fabs(xx  - *px);
	 		if (err > maxerr) 
	 			maxerr = err;
     		}

		xx += carry;
		zz = (xx+limitbv)-limitbv;
		carry = zz*invlimit;			// Compute the carry on next word
		*(px++) = xx - zz;				// And the balanced remainder in current word

	}

	if (carry)
	{
		j = 0;
		px = x;
		if (plus)
			carry = -carry;
		while (carry)
		{
			xx = *px + carry;
			zz = (xx+limitbv)-limitbv;
			carry = zz*invlimit;		// Compute the carry on next word
			*(px++) = xx - zz;			// And the balanced remainder in current word

			if (++j == N)
			{
				j = 0;
				px = x;
				if (plus)
					carry = -carry;
			}
		}
	}
	return(maxerr);
}

void									// Modular reduction of a zero padded integer
modred (
	double *x
)
{
	register double *pscr = scral;		// Init. ; Point to scratch area
	register double *px = x+FFTLEN-1; 	// Point to last FFT word
	register long hindex = hwcount; 	// Count of upper FFT words
	register double carry = 0.0, bv = BIGVAL, limitbv = (limit_high*bv)-bv, xx, yy, zz, q;

	if (debug)
		gwpstart_timer (3);

	*pscr++ = carry;					// Zero 12 double words in scratch area
	*pscr++ = carry;
	*pscr++ = carry;
	*pscr++ = carry;
	*pscr++ = carry;
	*pscr++ = carry;
	*pscr++ = carry;
	*pscr++ = carry;
	*pscr++ = carry;
	*pscr++ = carry;
	*pscr++ = carry;
	*pscr++ = carry;

	while (hindex-- > 0) {	// divide by mult the upper FFT words and save them in scratch area
		xx = *px+carry*limit_high;
		q = (xx*invmult+bv)-bv;		// q = xx/mult integer
		carry = xx-mult*q;			// carry = xx%h
		*px-- = 0.0;				// zero the high word
		*pscr++ = shift*q;			// save -inc*q*2^(ng-q)%bits
	}
	px++;
	pscr--;
	*px = carry;
	px = x;
	hindex = lwcount;
	carry = bv;
	while (hindex-- > 0) {	// Add or subtract saved words into the lower part of FFT
		xx = *px + *pscr-- + carry;			// xx = x + *pscr + carry
		yy = xx+limitbv;					// y = xx/limit_high*limit_high + bv
		zz = yy-limitbv;					// z = xx/limit_high*limit_high
		carry = limit_inverse_high*yy;		// carry = y / limit_high + bv
		*px++ = xx-zz;						// new x = xx%limit_high
	}
	px = x + hwoffset + 4;					// Point to 5th upper FFT word;
	carry = *px;
	*px-- = 0.0;

	xx = *px+carry*limit_high;
	q = (xx*invmult+bv)-bv;					// q = xx/mult integer
	carry = xx-mult*q;
	*px-- = 0.0;
	*pscr = shift*q;

	xx = *px+carry*limit_high;
	q = (xx*invmult+bv)-bv;					// q = xx/mult integer
	carry = xx-mult*q;
	*px-- = 0.0;
	*(pscr+1) = shift*q;

	xx = *px + carry*limit_high;
	q = (xx*invmult+bv)-bv;					// q = xx/mult integer
	carry = xx-mult*q;
	*px-- = 0.0;	// added
	*(pscr+2) = shift*q;

	xx = *px + carry*limit_high;
	q = (xx*invmult+bv)-bv;					// q = xx/mult integer
	carry = xx-mult*q;
	*px = 0.0;	// added
	*(pscr+3) = shift*q;
	
	while ((carry>(limit_high/2)) || (carry<(-limit_high/2))) {
		xx = carry+bv;						// Split the remainder
		yy = xx + limitbv;					// y = xx/limit_high*limit_high + bv
		zz = yy - limitbv;					// z = xx/limit_high*limit_high
		carry = limit_inverse_high*yy-bv;	// carry = y / limit_high
		*px++ = xx-zz;						// Save lower bits
	}
	*(px) = carry;
	px = x;								// Reload source pointer

	xx = *px + *(pscr+3) + bv;
	yy = xx + limitbv;					// y = xx/limit_high*limit_high + bv
	zz = yy - limitbv;					// z = xx/limit_high*limit_high
	carry = limit_inverse_high*yy;		// carry = y / limit_high
	*px++ = xx-zz;						// Save new value

	xx = *px + *(pscr+2) + carry;
	yy = xx + limitbv;					// y = xx/limit_high*limit_high + bv
	zz = yy - limitbv;					// z = xx/limit_high*limit_high
	carry = limit_inverse_high*yy;		// carry = y / limit_high
	*px++ = xx-zz;						// Save new value

	xx = *px + *(pscr+1) + carry;
	yy = xx + limitbv;					// y = xx/limit_high*limit_high + bv
	zz = yy - limitbv;					// z = xx/limit_high*limit_high
	carry = limit_inverse_high*yy;		// carry = y / limit_high
	*px++ = xx-zz;						// Save new value

	xx = *px + *(pscr+0) + carry;
	yy = xx + limitbv;					// y = xx/limit_high*limit_high + bv
	zz = yy - limitbv;					// z = xx/limit_high*limit_high
	carry = limit_inverse_high*yy;		// carry = y / limit_high
	*px++ = xx-zz;						// Save new value

	xx = *px + carry;
	yy = xx + limitbv;					// y = xx/limit_high*limit_high + bv
	zz = yy - limitbv;					// z = xx/limit_high*limit_high
	carry = limit_inverse_high*yy;		// carry = y / limit_high
	*px++ = xx-zz;						// Save new value

	*px	+= carry - bv;					// Adjust final carry

	if (debug)
		gwpend_timer (3);

}

void
check_balanced(						// Check if the balanced form of a result is correct
	double 	*x,
	int 	N
)
{
	int 	j;
	double 	lim, *ptrx = x;

	for (j=0; j<N; ++j)
	{
		lim = hlimit[j];
		assert ((*ptrx<=lim) && (*ptrx>=-lim));
		++ptrx;
	}
}

//	Funtions that do squaring and multiplications of large integers ; inputs and output are normalized

double
lucas_square(	// Squaring of a large integer ; input and output normalized
	double			*x,
	int 			N,
	int 			error_log,
	int				noadd,
	int				nomul
)
{
	register int 		j, hn = N/2;
	register double 	err;

	if (debug)
		gwpstart_timer (5);

	if (zp || generic)
		if (compl) {	// Transform an N sized real FFT array into an N/2 sized complex FFT one
			for (j=0; j<hn; ++j)
			{
				cxin[j][0] = x[j];
				cxin[j][1] = x[j+hn];
			}
		}
		else {
			for (j=0; j<N; ++j)
			{
				xin[j] = x[j];
			}
		}
	else
		if (compl) {	// Transform an N sized real FFT array into an N/2 sized complex FFT one
			for (j=0; j<hn; ++j)
			{
				cxin[j][0] = x[j] * two_to_phi[j];
				cxin[j][1] = x[j+hn] * two_to_phi[j+hn];
			}
		}
		else {
			for (j=0; j<N; ++j)
			{
				xin[j] = x[j] * two_to_phi[j];
			}
		}

	fftwsquare_g(N);	// DWT squaring

	if (zp || generic)
		if (compl) {	// Unfold the N/2 sized complex ouput array into a N sized real one
			for (j=0; j<hn; ++j)
			{
				x[j] = cxin[j][0] * ttmp;
				x[j+hn] = cxin[j][1] * ttmp;
			}
		}
		else {
			for (j=0; j<N; ++j)
			{
				x[j] = xin[j] * ttmp;
			}
		}
	else
		if (compl) {	// Unfold the N/2 sized complex ouput array into a N sized real one
			for (j=0; j<hn; ++j)
			{
				x[j] = cxin[j][0] *  two_to_minusphi[j];
				x[j+hn] = cxin[j][1] *  two_to_minusphi[j+hn];
			}
		}
		else {
			for (j=0; j<N; ++j)
			{
				x[j] = xin[j] *  two_to_minusphi[j];
			}
		}

	if (debug)
		gwpend_timer (5);

	if (debug)
		gwpstart_timer (2);
	err = (zp || generic)? rnormalize(x, N, error_log, noadd, nomul) : inormalize(x, N, error_log, noadd, nomul);
	if (debug)
		gwpend_timer (2);

	if (zp)
		modred (x);
	if (error_log)
		check_balanced(x, N);

	return(err);
}

// GPU version of above
double
lucas_square_gpu(	// Squaring of a large integer ; input and output normalized
	double			*x,
	int 			N,
	int 			error_log,
	int				noadd,
	int				nomul
)
{
	register int 		j, hn = N/2;
	register double 	err;

	gwpstart_timer(BSQR_TIMER);
	convolution_in(devDataIn, twoToPhi, x, FFTLEN);

	gwpend_timer(BSQR_TIMER);
	fftwsquare_g_gpu(N);	// DWT squaring
	gwpstart_timer(ASQR_TIMER);

	convolution_out(devDataIn, twoToMinusPhi, x, FFTLEN);

	gwpend_timer(ASQR_TIMER);

	gwpstart_timer(TRNS_TIMER);
	copyNumberOff(x, FFTLEN);
	gwpend_timer(TRNS_TIMER);

	gwpstart_timer(NORM_TIMER);
	err = (zp || generic)? rnormalize(x, N, error_log, noadd, nomul) : inormalize(x, N, error_log, noadd, nomul);
	// Using inormalize_cuda instead of inormalize won't work, but is here for completeness.
	//err = inormalize_cuda(devDataIn, BIGVAL, wrapindex, wrapfactor, addinindex, addinvalue, N);
	gwpend_timer(NORM_TIMER);

	gwpstart_timer(TRNS_TIMER);
	copyNumberOn(x, FFTLEN);
	gwpend_timer(TRNS_TIMER);

	return(err);
}

double
lucas_mul(							// Multiplication of large integers ; inputs and output normalized
	double	*x,
	double	*y,
	int 	N,
	int 	error_log,
	int		noadd,
	int		nomul
)
{
	register int 		j, hn = N/2;
	register double 	err;

	if (debug)
		gwpstart_timer (5);

	if (zp || generic)
		if (compl) {	// Transform N sized real FFT arrays into N/2 sized complex FFT ones
			for (j=0; j<hn; ++j)
			{
				cxin[j][0] = x[j];
				cxin[j][1] = x[j+hn];
				cyin[j][0] = y[j];
				cyin[j][1] = y[j+hn];
			}
		}
		else {
			for (j=0; j<N; ++j)
			{
				xin[j] = x[j];
				yin[j] = y[j];
			}
		}
	else
		if (compl) {	// Transform N sized real FFT arrays into N/2 sized complex FFT ones
			for (j=0; j<hn; ++j)
			{
				cxin[j][0] = x[j] * two_to_phi[j];
				cxin[j][1] = x[j+hn] * two_to_phi[j+hn];
				cyin[j][0] = y[j] * two_to_phi[j];
				cyin[j][1] = y[j+hn] * two_to_phi[j+hn];
			}
		}
		else {
			for (j=0; j<N; ++j)
			{
				xin[j] = x[j] * two_to_phi[j];
				yin[j] = y[j] * two_to_phi[j];
			}
		}

	fftwmul_g(N);	// DWT mutiplication

	if (zp || generic)
		if (compl) {	// Unfold the N/2 sized complex ouput array into a N sized real one
			for (j=0; j<hn; ++j)
			{
				y[j] = cyin[j][0] * ttmp;
				y[j+hn] = cyin[j][1] * ttmp;
			}
		}
		else {
			for (j=0; j<N; ++j)
			{
				y[j] = yin[j] * ttmp;
			}
		}
	else
		if (compl) {	// Unfold the N/2 sized complex ouput array into a N sized real one
			for (j=0; j<hn; ++j)
			{
				y[j] = cyin[j][0] *  two_to_minusphi[j];
				y[j+hn] = cyin[j][1] *  two_to_minusphi[j+hn];
			}
		}
		else {
			for (j=0; j<N; ++j)
			{
				y[j] = yin[j] *  two_to_minusphi[j];
			}
		}

	if (debug)
		gwpend_timer (5);

	if (debug)
		gwpstart_timer (2);
	err = (zp || generic)? rnormalize(y, N, error_log, noadd, nomul) : inormalize(y, N, error_log, noadd, nomul);
	if (debug)
		gwpend_timer (2);

	if (zp)
		modred (y);
	if (error_log)
		check_balanced(y, N);

	return(err);
}

/* ------------ Initialization routines ------------------- */


int set_fftlen (double k, unsigned long b, unsigned long n) {
	double bpw, tbpw, kbits = 0, rdwt, rzpad; 
	double log2k, log2b, bitsmc = log(MAXMULCONST)/log(2.0);
	int incfft, fftwincr, fftdwt = 2, fftzpad = 2 , zpad = 0;
	unsigned long len = bitlen (gmodulus);

	printfunction = (verbose)? both_output : screen_output;
	rzpad = len + len + 32;	// Exponent for zero padded or generic modes
	if (generic)
		rzpad += 2*EB;

	while (1) {			// Compute zero padded or generic FFT length (raw)
		bpw = (rzpad+fftzpad-1)/fftzpad;
		tbpw = 2.0*bpw + bitsmc + ((generic)? 2.0 : 0);
		if (tbpw <= MAXBITSPERDOUBLE)
			break;
		else
			fftzpad *= 2;
	}

	incfft = FFTINC;
	while ((fftzpad <= 64) && (incfft-- > 0))
		fftzpad *= 2;

	if (fftzpad > 64) {
		fftzpad /= 2;
		fftwincr = fftzpad / 16;
		while (1) {		// Compute zero padded or generic FFT length (fine)
			bpw = (rzpad+fftzpad-1)/fftzpad;
			tbpw = 2.0*bpw + bitsmc + ((generic)? 2.0 : 0);
			if (tbpw <= MAXBITSPERDOUBLE)
				break;
			else
				fftzpad += fftwincr;
		}
		while (incfft-- > 0)
			fftzpad += fftwincr;
	}

		

	if (k > 0.0) {
		log2k = log(k)/log(2.0);
		log2b = log(b)/log(2.0);
		kbits = log2k + 1;
		rdwt = n*log2b + log2k;	// Exponent for DWT mode

		while (1) {			// Compute DWT FFT length (raw)
			bpw = (rdwt+fftdwt-1)/fftdwt;
			tbpw = 2.0*bpw + kbits + bitsmc + 0.6*log((double)fftdwt)/log(2.0);
    			if (tbpw <= MAXBITSPERDOUBLE || bpw < 5.0)
				break;
			else
				fftdwt *= 2;
		}
		incfft = FFTINC;
		while ((fftdwt <= 64) && (incfft-- > 0))
			fftdwt *= 2;

		if (fftdwt > 64) {
			fftdwt /= 2;
			fftwincr = fftdwt / 16;
			while (1) {		// Compute DWT FFT length (fine)
				bpw = (rdwt+fftdwt-1)/fftdwt;
				tbpw = 2.0*bpw + kbits + bitsmc + 0.6*log((double)fftdwt)/log(2.0);
    				if (tbpw <= MAXBITSPERDOUBLE || bpw < 5.0)
					break;
				else
				fftdwt += fftwincr;
			}
			while (incfft-- > 0)
				fftdwt += fftwincr;
		}
	}


	if (generic) {
		zpad = 0;
		FFTLEN = fftzpad;
		bpw = RINT((rzpad+fftzpad-1)/fftzpad);
		ng = (unsigned long)bpw*FFTLEN;
		kg = 1.0;
		grecip = newgiant (FFTLEN*sizeof(double)/sizeof(short) + 16);
		itog (1, grecip); 
		gshiftleft (len + len + EB, grecip); 
		divg (gmodulus, grecip);					/* computes len+EB+1 bits of reciprocal */ 
		gshiftleft (ng - len - len - EB, grecip);	/* shift so gwmul routines wrap */ 
													/* quotient to lower end of fft */ 
	    zerowordslow = (len - EB) / (unsigned long)bpw; 
	    zerowordshigh = FFTLEN - (len + (unsigned long)bpw - 1) / (unsigned long)bpw - 1; 
	}
	else if (b != 2 || kbits > MAXKBITS || kbits  > MAXBITSPERDOUBLE || fftdwt > fftzpad || bpw < 5.0) {
		zpad = 1;
		FFTLEN = fftzpad;
		bpw = RINT((rzpad+fftzpad-1)/fftzpad);
		ng = (unsigned long)bpw*FFTLEN;
		kg = 1.0;
	}
	else {
		zpad = 0;
		bpw = (rdwt+fftdwt-1)/fftdwt;
		FFTLEN = fftdwt;
	}
	int g_fftlen = 0;
	{
		int ii,jj;
		if(g_fftlen != 0) FFTLEN = g_fftlen * 2 + 2 ;
   		for(jj = ii = 64;ii < (FFTLEN >> 1) ;ii*=2)
        	for(jj=ii;jj < (FFTLEN >> 1) && jj < ii*2 ;jj+=ii/4);
    		FFTLEN = jj;
    		g_fftlen = jj;
		{
			if(g_fftlen != 0) FFTLEN = g_fftlen * 2 + 2 ;
   			for(jj = ii = 64;ii < (FFTLEN >> 1) ;ii*=2)
        		for(jj=ii;jj < (FFTLEN >> 1) && jj < ii*2 ;jj+=ii/4);
    			FFTLEN = jj;
    			g_fftlen = jj;
		}
	}

	tbpw = (zpad || generic) ? 2.0*bpw + bitsmc : 2.0*bpw + kbits + bitsmc + 0.6*log((double)FFTLEN)/log(2.0);

	if (debug) {
		sprintf (gwpbuf, "FFTLEN = %d, bpw = %f, Bits per double = %f, Maxbpd = %f\n",
		FFTLEN, bpw, tbpw, MAXBITSPERDOUBLE);
		if (printfunction != NULL)
			(*printfunction)(gwpbuf);
	}
	return (zpad);
}

void gwpset_larger_fftlen_count(
	int count
)
{
	FFTINC = count;
}

void	init_fftw (int n) {
	int j, hn = n/2;

	if (compl) {
		cxin = fftw_malloc (hn * sizeof(fftw_complex));
		cyin = fftw_malloc (hn * sizeof(fftw_complex));
		cxout = fftw_malloc (hn * sizeof(fftw_complex));
		cyout = fftw_malloc (hn * sizeof(fftw_complex));
		fwpx = fftw_plan_dft_1d (hn, cxin, cxout, FFTW_FORWARD, FFTW_PATIENT);
		fwpy = fftw_plan_dft_1d (hn, cyin, cyout, FFTW_FORWARD, FFTW_PATIENT);
		bwpx = fftw_plan_dft_1d (hn, cxout, cxin, FFTW_BACKWARD, FFTW_PATIENT);
		bwpy = fftw_plan_dft_1d (hn, cyout, cyin, FFTW_BACKWARD, FFTW_PATIENT);
	}
	else {
		xin = fftw_malloc (n * sizeof(double));
		yin = fftw_malloc (n * sizeof(double));
		cxout = fftw_malloc ((hn+1) * sizeof(fftw_complex));
		cyout = fftw_malloc ((hn+1) * sizeof(fftw_complex));
		fwpx = fftw_plan_dft_r2c_1d (n, xin, cxout, FFTW_PATIENT);
		fwpy = fftw_plan_dft_r2c_1d (n, yin, cyout, FFTW_PATIENT);
		bwpx = fftw_plan_dft_c2r_1d (n, cxout, xin, FFTW_PATIENT);
		bwpy = fftw_plan_dft_c2r_1d (n, cyout, yin, FFTW_PATIENT);
		initcufft(FFTLEN);
		cudaMemcpy(twoToPhi, two_to_phi, FFTLEN * sizeof(cufftDoubleReal), cudaMemcpyHostToDevice);
		cudaMemcpy(twoToMinusPhi, two_to_minusphi, FFTLEN * sizeof(cufftDoubleReal), cudaMemcpyHostToDevice);
	}

	if (compl) {
		cnp = (double *)malloc(n*sizeof(double));
		snp = (double *)malloc(n*sizeof(double));
		for (j=0;j<n;j++)
		{
			cnp[j] = fftcosp(j);
			snp[j] = fftsinp(j);
		}
	}
}

// Get the pointers to the user output functions

void gwpsetoutputs (
	void(*screenf)(char *),
	void(*bothf)(char *)
)
{
	screen_output = screenf;
	both_output = bothf;
}

// Initialize the gwpnum system

int
gwpsetup(
	double	k,			// The multiplier
	unsigned long	b,	// The base (force generic reduction if not two)
	unsigned long 	n,	// The exponent
	signed long 	c,	// c, in k*b^n+c (force generic reduction if not +1 or -1)
	giant modulus_arg	// The modulus of the modular reduction
)
{
	long j, len, g, limit;
	double 	log2;
	double	log2k, log2b;
	double	tc1 = 12345.6789, tc2 = 6789.12345;

	printfunction = (verbose)? both_output : screen_output;
	gmodulus = modulus_arg;

	if (abs(c) != 1)
		generic = 1;
	else
		plus = compl = (c==1)? 1 : 0;

	log2 = log(2.0);
	log2b = log(b)/log2;

#define logb(x) log(x)/log(b)

	if (k > 0.0) {
		log2k = log(k)/log2;
		r = (double)n*log2b + log2k;
	}
	else
		r = (double)n*log2b;

	if (b != 2 || k == 0.0 || k*MAXMULCONST > MAX_ZEROPAD_K) {
		generic = 1;
	}

	MAXERR = 0.0;

	BIGVAL = 3.0;
	while (RINTP(tc1) != RINT(tc1) || RINTP(tc2) != RINT(tc2)) {
		BIGVAL *= 2.0;
	}
	while (RINTP(tc1) == RINT(tc1) && RINTP(tc2) == RINT(tc2)) {
		BIGVAL *= 2.0;
	}
	BIGVAL /= 2.0;

/* Calculate the number of bits in k*2^n.  This will be helpful in */
/* determining how much memory to allocate for giants. */

	bit_length = bitlen (gmodulus);

	if (debug) {
		sprintf (gwpbuf, "k = %14.1f, b = %d, n = %d, c = %d, bit_length = %d\n", k, b, n, c, bit_length);
		if (printfunction != NULL)
			(*printfunction)(gwpbuf);
	}


	ng = n;
	kg = k;
	zp = set_fftlen(k, b, n);

	avg_num_b_per_word = ((zp || generic) ? n * 2.0 : (logb(k) + n)) / FFTLEN;



	if (zp) {
		inc = c;		// copy for zp
		bits = ng/FFTLEN;
		limit_high = (double)(1<<bits);
		limit_inverse_high = 1.0/limit_high;
		limit_high_bigval = (limit_high*BIGVAL)-BIGVAL;
		mult = k;		// copy for zp
		invmult = 1.0/mult;
		rem = (ng - n)%bits;
		shift = (double) (1L << rem) * -inc;
		hwcount = (ng - n)/bits;
		lwcount = (((ng - n)/bits + 9)/8)*8;
		hwoffset = (n + bits - 1) / bits;
		if (debug) {
			sprintf (gwpbuf, "bits = %d, n = %d, hwcount = %d, lwcount = %d, hwoffset = %d\n", bits, ng, hwcount, lwcount, hwoffset);
			if (printfunction != NULL)
				(*printfunction)(gwpbuf);
			sprintf (gwpbuf, "limit_high = %g, limit_inverse_high = %g, limit_high_bigval = %g, mult = %g, invmult = %g, rem = %d, shift = %g\n",
				limit_high, limit_inverse_high, limit_high_bigval, mult, invmult, rem, shift);
			if (printfunction != NULL)
				(*printfunction)(gwpbuf);
		}
		temp = (long) malloc ((hwcount + 24) * sizeof (double) + 256);
		scr = (double *) temp;							// address of scratch area
		scral = (double *) ((temp + 7) & 0xFFFFFFFFFFFFFFF8);	// id. double word aligned
		k = kg;
		n = ng;
		c = (zcomplex)? 1 : -1;
		plus = compl = (c == 1)? 1 : 0;
	}
	else if (generic) {
		k = kg;
		n = ng;
		c =(zcomplex)? 1 : -1;
		plus = compl = (c == 1)? 1 : 0;
		bits = ng/FFTLEN;
		limit_high = (double)(1<<bits);
		limit_inverse_high = 1.0/limit_high;
	}

	gwfft_weight_setup (0, k, n, c, FFTLEN);
	len = (FFTLEN+1)*sizeof(double);
	if (fftbase == NULL)
		fftbase = (int *)malloc((FFTLEN+1)*sizeof(int));
	if (two_to_phi == NULL)
		two_to_phi = (double *)malloc(len);
	if (two_to_minusphi == NULL)
		two_to_minusphi = (double *)malloc(len);
	if (invlimit == NULL)
		invlimit = (double *)malloc(len);
	if (flimit == NULL)
		flimit = (double *)malloc(len);
	if (hlimit == NULL)
		hlimit = (double *)malloc(len);
	if (limitbv == NULL)
		limitbv = (double *)malloc(len);
	high = (double)(1<<gwfft_base(1));
	low = 0.5*high;
	last = 1<<(gwfft_base(FFTLEN)-gwfft_base(FFTLEN-1));
	highinv = 1.0/high;
	lowinv = 1.0/low;
	lastinv = 1.0/last;
	if (debug) {
		sprintf (gwpbuf, "clog2k = %d, log2k = %7.4f\n", (int)ceil(log2k), log2k);
		if (printfunction != NULL)
			(*printfunction)(gwpbuf);
	}
	wrapindex = 0;
	wrapfactor = 1.0;
	g = 1;
	if (k != 1.0) {
		g = (1<<(int)ceil(log2k))-(int)k;
		while (n > gwfft_base(wrapindex))
			wrapindex++;
		wrapindex--;
		wrapfactor = g;
		for (j=0;j+gwfft_base(wrapindex)<n;j++)
			wrapfactor *= 2.0;
	}
	if (debug) {
		sprintf (gwpbuf, "wrapindex = %d, wrapfactor = %f\n", wrapindex, wrapfactor);
		if (printfunction != NULL)
			(*printfunction)(gwpbuf);
		sprintf(gwpbuf, "INIT : r = %7.4f, low = %7.4f, high = %7.4f, last = %7.4f, g = %d, B = %g\n",
		r, low, high, last, g, BIGVAL);
		if (printfunction != NULL)
			(*printfunction)(gwpbuf);
	}
	for(j=0; j<FFTLEN; ++j) {
		two_to_phi[j] = gwfft_weight (j);
		two_to_minusphi[j] = (compl) ? 2.0*gwfft_weight_inverse_over_fftlen (j) :
						gwfft_weight_inverse_over_fftlen (j);
		fftbase[j] = gwfft_base(j+1) - gwfft_base(j);
	}
	ttmp = two_to_minusphi[0];
	flimit[0] = high;
	invlimit[0] = highinv;
	hlimit[0] = low;
	limitbv[0] = high*BIGVAL-BIGVAL;
	flimit[FFTLEN-1] = last;
	invlimit[FFTLEN-1] = lastinv;
	hlimit[FFTLEN-1] = 0.5*last;
	limitbv[FFTLEN-1] = last*BIGVAL-BIGVAL;
	for(j=1; j<FFTLEN-1; ++j) {
		limit = 1<<(gwfft_base(j+1) - gwfft_base(j));
		flimit[j] = limit;
		invlimit[j] = 1.0/limit;
		hlimit[j] = 0.5*limit;
		limitbv[j] = limit*BIGVAL-BIGVAL;
	}
	if (generic) {
		modulus = gwpalloc ();
		recip = gwpalloc ();
		gwptmp = gwpalloc ();
		gianttogwp (grecip, recip);
		gianttogwp (gmodulus, modulus);
	}
	init_fftw(FFTLEN);
	addinindex = 0;
	addinvalue = 0.0;
	SMALLMULCONST = 1.0;
	MULBYCONST = 0;
	return 0;
}

int gwpsetup_general_mod_giant (
	giant modulus_arg	// The modulus of the modular reduction
)
{
	return gwpsetup (0.0, 1, 1, -1, modulus_arg);
}

//	Miscellanous utility routines

void bitaddr (
	unsigned long bit,
	unsigned long *word,
	unsigned long *bit_in_word,
	unsigned long FFTLEN)
{

/* What word is the bit in? */

	*word = (unsigned long) ((double) bit / r * FFTLEN);
	if (*word >= FFTLEN) *word = FFTLEN - 1;

/* Compute the bit within the word. */

	*bit_in_word = bit - (int)ceil(*word*r/FFTLEN);
}

void setaddin (
	long	value, int N)
{
	unsigned long word, bit_in_word;

	if (zp || generic) {
		addinvalue = (double) value;
		addinindex = 0;
		return;
	}

/* If value is even, shift it right and increment bit number.  This */
/* will ensure that we modify the proper FFT word. */

	for (bit_in_word = 0; value && (value & 1) == 0; value >>= 1)
		bit_in_word++;

/* Convert the input value to 1/k format.  Case 1 (2^n+/-1: Inverse of k */
/* is 1.  Case 2 (k*2^n-1): Inverse of k is 2^n.  Case 3 (k*2^n+1): Inverse */
/* of k is -2^n.  No other cases can be handled. */

	if (kg == 1.0) {
		bitaddr (bit_in_word, &word, &bit_in_word, N);
	}
	else {
		bitaddr (ng + bit_in_word, &word, &bit_in_word, N);
	}
	addinvalue = (plus && (kg != 1.0)) ? -(double)(value<<bit_in_word) : (double)(value<<bit_in_word);
	addinindex = word;
}

/* Add a small constant at the specified power of b after the */
/* next multiplication.  That is, value*b^power_of_b is added to */
/* the next multiplication result.  This only works if k=1. */

void gwpsetaddinatpowerofb (
	long	value,
	unsigned long power_of_b)
{
	unsigned long word, b_in_word;

/* If value is even, shift it right and increment bit number.  This */
/* will ensure that we modify the proper FFT word. */

	for (power_of_b; value && (value & 1) == 0; value >>= 1)
		power_of_b++;

	bitaddr (power_of_b, &word, &b_in_word, FFTLEN);

	addinvalue = (double)(value<<b_in_word);
	addinindex = word;
}

/* Test if a gwpnum is zero */

#define	MAX_NZ_COUNT 10

int
gwpiszero(
	gwpnum 	gg
)
{
	register long 		j, count = 0;
	register double 	*gp = gg;
	long	result;
	giant	gtest;

	if (!generic)
		for(j=0; j<FFTLEN; j++) {

			if (count > MAX_NZ_COUNT)
				return 0;		// Too much non zero words, the gwpnum is not zero.
			else if (*gp++)
				count++;		// Update the count of non-zero words.
		}
	if (count || generic) {		// The gwpnum is likely to be zero but needs a more accurate test...
		gtest = newgiant (FFTLEN*sizeof(double)/sizeof(short) + 16);// Allocate memory for the giant
		gwptogiant (gg, gtest);
		result = isZero (gtest);
		gwpfree (gtest);			// Free memory
		return (result);
	}
	else
		return 1;			// The gwpnum is zero
}

/* Test two gwpnums for equality */

int	gwpequal (
	gwpnum gw1, 
	gwpnum gw2
) 
{
	gwpnum gwdiff;
	int result;

	gwdiff = gwpalloc ();			// Reserve memory for the difference
	gwpsub3 (gw1, gw2, gwdiff);		// Normalized subtract...
	result = gwpiszero (gwdiff);	// Test for zero difference
	gwpfree (gwdiff);				// Free memory
	return (result);
}


//	User side functions...

double
gwpnormalize(
	gwpnum s
)
{
	double err;

	if (debug)
		gwpstart_timer (2);
	err = (zp || generic)? rnormalize(s, FFTLEN, E_CHK, 0, 0) : inormalize(s, FFTLEN, E_CHK, 0, 0);
	if (debug)
		gwpend_timer (2);
	return (err);
}


double
gwprawnormalize(
	gwpnum s
)
{
	double err;

	if (debug)
		gwpstart_timer (2);
	err = (zp || generic)? rnormalize(s, FFTLEN, E_CHK, 1, 1) : inormalize(s, FFTLEN, E_CHK, 1, 1);
	if (debug)
		gwpend_timer (2);
	return (err);
}

/* Routine that copy a gwpnum from */
/* source to dest while zeroing some lower FFT words */

void gwpcopyzero (
	gwpnum	s,
	gwpnum	d,
	unsigned long n)
{
	register double zero = 0.0;
	register double *sptr = s + n;
	register double *dptr = d;
	register double *maxptr;

	if (debug)
		gwpstart_timer (4);
	maxptr = d + n;
	while (dptr < maxptr)
		*dptr++ = zero;

	maxptr = d + FFTLEN;
	while (dptr < maxptr)
		*dptr++ = *sptr++;
	if (debug)
		gwpend_timer (4);
}

/* Set a gwpnum to zero */

void gwpzero (gwpnum s) {
	long j;

	for(j=0; j<FFTLEN; ++j)
		s[j] = 0;
	return;
}

/* Routine that zero some high words in a gwpnum */

void gwpsetzero (
	gwpnum s,
	unsigned long n)
{
	register double zero = 0.0;
	register double *sptr = s + FFTLEN - n;
	register double *maxptr = s + FFTLEN;

	if (debug)
		gwpstart_timer (4);
while (sptr < maxptr)
	*sptr++ = zero;
	if (debug)
		gwpend_timer (4);
}

void generic_modred(
	gwpnum s
)
{
	double err;

	gwpcopyzero (s, gwptmp, zerowordslow);
	err = lucas_mul (recip, gwptmp, FFTLEN, E_CHK, 1, 1);
	if (err > MAXERR)
		MAXERR = err;
	gwpsetzero (gwptmp, zerowordshigh);
	err = lucas_mul (modulus, gwptmp, FFTLEN, E_CHK, 1, 1);
	if (err > MAXERR)
		MAXERR = err;
	if (debug)
		gwpstart_timer (4);
	if (compl) {
		gwpaddquick (gwptmp, s);
		gwpsubquick (modulus, s);
	}
	else
		gwpsubquick (gwptmp, s);
	if (debug)
		gwpend_timer (4);
}

void gwpfft_description (
	char *buf)
{
	if (zp)
		sprintf (buf, "Using zero-padded rational base DWT, FFT length = %d", FFTLEN);
	else if (generic)
		sprintf (buf, "Using rational base DWT and generic reduction, FFT length = %d", FFTLEN);
	else if (compl)
		sprintf (buf, "Using complex irrational base DWT, FFT length = %d", FFTLEN);
	else
		sprintf (buf, "Using real irrational base DWT, FFT length = %d", FFTLEN);
}



// User side large integers arithmetic operations


void gwpcopy (
	gwpnum s,
	gwpnum d)
{
	int i;

	for (i=0; i<FFTLEN; i++)
		d[i] = s[i];
}


void gwpaddquick (
	gwpnum s,
	gwpnum d)
{
	int i;

	for (i=0; i<FFTLEN; i++)
		d[i] += s[i];
}


void gwpsubquick (
	gwpnum s,
	gwpnum d)
{
	int i;

	for (i=0; i<FFTLEN; i++)
		d[i] -= s[i];
}


void gwpadd (
	gwpnum s,
	gwpnum d)
{
	int i;

	for (i=0; i<FFTLEN; i++)
		d[i] += s[i];
	gwprawnormalize (d);
//	if (zp)
//		modred (d);
//	else if (generic)
//		generic_modred (d);
}


void gwpsub (
	gwpnum s,
	gwpnum d)
{
	int i;

	for (i=0; i<FFTLEN; i++) {
		d[i] -= s[i];
	}
	gwprawnormalize (d);
//	if (zp)
//		modred (d);
//	else if (generic)
//		generic_modred (d);
}

void gwpadd3 (
	gwpnum s1,
	gwpnum s2,
	gwpnum d)
{
	int i;

	for (i=0; i<FFTLEN; i++)
		d[i] = s1[i] + s2[i];
	gwprawnormalize (d);
//	if (zp)
//		modred (d);
//	else if (generic)
//		generic_modred (d);
}


void gwpsub3 (
	gwpnum s1,
	gwpnum s2,
	gwpnum d)
{
	int i;

	for (i=0; i<FFTLEN; i++)
		d[i] = s1[i] - s2[i];
	gwprawnormalize (d);
//	if (zp)
//		modred (d);
//	else if (generic)
//		generic_modred (d);
}

void
gwpsquare (gwpnum s) {
	double err;

	err = lucas_square (s, FFTLEN, E_CHK, 0, 0);
	if (err > MAXERR)
		MAXERR = err;
	if (generic)
		generic_modred (s);
}

void
gwpsquare_gpu(gwpnum s)
{
	double err;

	err = lucas_square_gpu(s, FFTLEN, E_CHK, 0, 0);
	if (err > MAXERR)
		MAXERR = err;
}

void
gwpmul (gwpnum s, gwpnum d) {
	double err;

	err = lucas_mul (s, d, FFTLEN, E_CHK, 0, 0);
	if (err > MAXERR)
		MAXERR = err;
	if (generic)
		generic_modred (d);
}

/* Generate random FFT data */

void gwp_random_number (
	gwpnum	x)
{
	giant	g;
	unsigned long i, len;

/* Generate the random number */

	srand ((unsigned int) time (NULL));
	len = (unsigned long) (bit_length / 16) + 1;
	g = newgiant (len);
	for (i = 0; i < len; i++) {
		g->n[i] = ((unsigned short) rand() << 10) +
			  ((unsigned short) rand() << 5) +
			  (unsigned short) rand();
	}
	g->sign = len;
	modg (gmodulus, g);
	gianttogwp (g, x);
	gwprawnormalize (x);
	gwpfree(g);
}


/* Square a number using a slower method that will have reduced */
/* round-off error on non-random input data.*/

void gwpsquare_carefully (
	gwpnum	s)		/* Source and destination */
{
	gwpnum	tmp1, tmp2;
	double	err;

/* Generate a random number, if we have't already done so */

	if (GWP_RANDOM == NULL) {
		GWP_RANDOM = gwpalloc ();
		gwp_random_number (GWP_RANDOM);
	}

/* Now do the squaring using three multiplies and adds */

	tmp1 = gwpalloc ();
	tmp2 = gwpalloc ();
	gwpadd3 (s, GWP_RANDOM, tmp1);		/* Compute s+random */
	gwpcopy (GWP_RANDOM, tmp2);
	err = lucas_mul (tmp2, s, FFTLEN, E_CHK, 1, 0);	/* Compute s*random without adding*/
	if (err > MAXERR)
		MAXERR = err;
	if (generic)
		generic_modred (s);
	err = lucas_square (tmp2, FFTLEN, E_CHK, 1, 0);	/* Compute random^2 without addin*/
	if (err > MAXERR)
		MAXERR = err;
	if (generic)
		generic_modred (tmp2);
	gwpsquare (tmp1);					/* Compute (s+random)^2  + addinvalue */
	gwpsubquick (tmp2, tmp1);			/* Calc s^2 from 3 results */
	gwpaddquick (s, s);
	gwpsub3 (tmp1, s, s);

/* Free memory and return */

	gwpfree (tmp1);
	gwpfree (tmp2);
}


/* Multiply numbers using a slower method that will have reduced */
/* round-off error on non-random input data.*/

void gwpmul_carefully (
	gwpnum s, gwpnum t)					/* Source and destination */
{
	gwpnum	tmp1, tmp2, tmp3, tmp4;
	double	err;

/* Generate a random number, if we have't already done so */

	if (GWP_RANDOM == NULL) {
		GWP_RANDOM = gwpalloc ();
		gwp_random_number (GWP_RANDOM);
	}

/* Now do the multiply using four multiplies and adds */

	tmp1 = gwpalloc ();
	tmp2 = gwpalloc ();
	tmp3 = gwpalloc ();
	tmp4 = gwpalloc ();
	gwpcopy (s, tmp4);
	gwpadd3 (s, GWP_RANDOM, tmp1);		/* Compute s+random */
	gwpadd3 (t, GWP_RANDOM, tmp3);		/* Compute t+random */
	gwpcopy (GWP_RANDOM, tmp2);
	err = lucas_mul (tmp2, tmp4, FFTLEN, E_CHK, 1, 0);	/* Compute s*random without adding*/
	if (err > MAXERR)
		MAXERR = err;
	if (generic)
		generic_modred (tmp4);
	err = lucas_mul (tmp2, t, FFTLEN, E_CHK, 1, 0);		/* Compute t*random without adding*/
	if (err > MAXERR)
		MAXERR = err;
	if (generic)
		generic_modred (t);
	err = lucas_square (tmp2, FFTLEN, E_CHK, 1, 0);		/* Compute random^2 without addin*/
	if (err > MAXERR)
		MAXERR = err;
	if (generic)
		generic_modred (tmp2);
	err = lucas_mul (tmp1, tmp3, FFTLEN, E_CHK, 0, 0);	/* Compute (s+random)*(t+random) + addinvalue */
	if (err > MAXERR)
		MAXERR = err;
	if (generic)
		generic_modred (tmp3);
	gwpsubquick (tmp2, tmp3);				/* Subtract random^2 */
	gwpsubquick (t, tmp3);
	gwpsub3 (tmp3, tmp4, t);

/* Free memory and return */

	gwpfree (tmp1);
	gwpfree (tmp2);
	gwpfree (tmp3);
	gwpfree (tmp4);
}


/* Do a squaring very carefully.  This is done after a normal */
/* iteration gets a roundoff error above 0.40.  This careful iteration */
/* will not generate a roundoff error. */

void gwpcareful_squaring (
	gwpnum s
)		
{
	gwpnum	hi, lo;
	long i;
	double	err;

/* Copy the data to hi and lo.  Zero out half the FFT data in each. */

	hi = gwpalloc ();
	lo = gwpalloc ();
	gwpcopy (s, hi);
	gwpcopy (s, lo);
	for (i = 0; i < FFTLEN/2; i++)
		hi[i] = 0;
	for ( ; i < FFTLEN; i++)
		lo[i] = 0;

/* Now do the squaring using three multiplies and adds */

	gwpcopy (hi, s);
	err = lucas_mul (lo, s, FFTLEN, E_CHK, 1, 0);	// Compute hi*lo without adding
	if (err > MAXERR)
		MAXERR = err;
	if (generic)
		generic_modred (s);
	gwpsquare (hi);									// Compute hi^2 + addin value
	err = lucas_square (lo, FFTLEN, E_CHK, 1, 0);	// Compute lo^2 without addin
	if (err > MAXERR)
		MAXERR = err;
	if (generic)
		generic_modred (lo);
	gwpaddquick (s, s);				// 2*hi*lo
	gwpaddquick (hi, s);			// + hi^2
	gwpadd (lo, s);					// + lo^2

/* Free memory and return */

	gwpfree (hi);
	gwpfree (lo);
}


void gwpcareful_multiply (
	gwpnum s,
	gwpnum t
)		
{
	gwpnum	hi, lo, hi2, lo2, u;
	long i;
	double	err;

/* Copy the data to hi, lo, hi2 and lo2.  Zero out half the FFT data in each. */

	hi = gwpalloc ();
	lo = gwpalloc ();
	hi2 = gwpalloc ();
	lo2 = gwpalloc ();
	u = gwpalloc ();

	gwpcopy (s, hi);
	gwpcopy (s, lo);
	gwpcopy (t, hi2);
	gwpcopy (t, lo2);

	for (i = 0; i < FFTLEN/2; i++) {
		hi[i] = 0;
		hi2[i] = 0;
	}

	for ( ; i < FFTLEN; i++) {
		lo[i] = 0;
		lo2[i] = 0;
	}

/* Now do the multiply using four multiplies and adds */

	gwpcopy (hi2, t);
	gwpmul (hi, t);									// Compute hi*hi2 + addin value
	gwpcopy (lo2, u);
	gwpmul (lo, u);
	err = lucas_mul (lo, u, FFTLEN, E_CHK, 1, 0);	// Compute lo*lo2 without adding
	if (err > MAXERR)
		MAXERR = err;
	if (generic)
		generic_modred (u);
	gwpmul (lo2, hi);
	err = lucas_mul (lo2, hi, FFTLEN, E_CHK, 1, 0);	// Compute lo2*hi without adding
	if (err > MAXERR)
		MAXERR = err;
	if (generic)
		generic_modred (hi);
	gwpmul (hi2, lo);
	err = lucas_mul (hi2, lo, FFTLEN, E_CHK, 1, 0);	// Compute hi2*lo without adding
	if (err > MAXERR)
		MAXERR = err;
	if (generic)
		generic_modred (lo);
	gwpaddquick (u, t);
	gwpaddquick (hi, t);
	gwpadd (lo, t);

/* Free memory and return */

	gwpfree (hi);
	gwpfree (lo);
	gwpfree (hi2);
	gwpfree (lo2);
	gwpfree (u);
}

// Set small add-in constant

void gwpsetaddin(
	long s
)
{
	setaddin (s, FFTLEN);
}

// Set small multiplicative constant

void gwpsetmulbyconst(
	long s
)
{
	SMALLMULCONST = (double)s;
}

// Set the maximum of the multiplicative constant

void gwpsetmaxmulbyconst(
	long s
)
{
	MAXMULCONST = (double)s;
}

// Conversion routines

void itogwp(					// Set a gwpnum to a small value
	int s,
	gwpnum d
)
{
	int j, saveindex;
	double savevalue;

	for (j=0; j<FFTLEN; j++)	// Init large integer to zero
		d[j] = 0.0;
	saveindex = addinindex;		// Save setaddin internal status
	savevalue = addinvalue;
	setaddin(s, FFTLEN);
	d[addinindex] = addinvalue;	// Set the large integer value in 1/k format
	addinindex = saveindex;		// Restore setaddin
	addinvalue = savevalue;
}

void gwpaddsmall(				// Add a small value to a gwpnum
	gwpnum d,
	int s
)
{
	int saveindex;
	double savevalue;

	saveindex = addinindex;		// Save setaddin internal status
	savevalue = addinvalue;
	setaddin (s, FFTLEN);
	gwpnormalize(d);			// Normalize the number while adding the value
	addinindex = saveindex;		// Restore setaddin
	addinvalue = savevalue;
}

/* Convert a giant to gwpnum FFT format.  Giant must be a positive number. */

void gianttogwp (
	giant	a,
	gwpnum	g)
{
	giant	newg;
	unsigned e1len;
	int	i, bits_in_next_binval;
	unsigned long binval, carry;
	unsigned short *e1;

/* To make the mod k*b^n+c step faster, gwpnum's are pre-multiplied by 1/k */
/* If k is greater than 1, then we calculate the inverse of k, multiply */
/* the giant by the inverse of k, and do a mod k*b^n+c. */

	if (kg > 1) {

		newg = newgiant (((unsigned long)( bit_length / 16) + 1) * 2);

		/* Easy case 1 (k*2^n-1): Inverse of k is 2^n */

		if (!plus) {
			gtog (a, newg);
			gshiftleft (ng, newg);
		}

		/* Easy case 2 (k*2^n+1): Inverse of k is -2^n */

		else {
			gtog (gmodulus, newg);	// replace -a by gmodulus-a to have positive numbers!!
			subg (a, newg);
			gshiftleft (ng, newg);
		}

		modg (gmodulus, newg);

		a = newg;
	}

/* Now convert the giant to FFT format */

	ASSERTG (a->sign >= 0);
	e1len = a->sign;
	e1 = a->n;

	if (e1len) {binval = *e1++; e1len--; bits_in_next_binval = 16;}
	else binval = 0;
	carry = 0;
	for (i = 0; i < FFTLEN; i++) {
		int	bits;
		long	value, mask;
		bits = (zp || generic)? ng/FFTLEN : fftbase [i];
		mask = (1L << bits) - 1;
		if (i == FFTLEN - 1) value = binval;
		else value = binval & mask;
		value = value + carry;
		if (value > (mask >> 1) && bits > 1 && i != (FFTLEN - 1)) {
			value = value - (mask + 1);
			carry = 1;
		} else {
			carry = 0;
		}
		g[i] = (double)value;

		binval >>= bits;
		if (e1len == 0) continue;
		if (bits_in_next_binval < bits) {
			if (bits_in_next_binval)
				binval |= (*e1 >> (16 - bits_in_next_binval)) << (16 - bits);
			bits -= bits_in_next_binval;
			e1++; e1len--; bits_in_next_binval = 16;
			if (e1len == 0) continue;
		}
		if (bits) {
			binval |= (*e1 >> (16 - bits_in_next_binval)) << (16 - bits);
			bits_in_next_binval -= bits;
		}
	}

/* Free allocated memory */

	if (kg > 1)
		gwpfree(newg);
}


int gwptogiant (
	gwpnum	gg,
	giant	v)
{
	long	val;
	int	i, j, limit, bits, bitsout, carry;
	unsigned short *outptr;

	limit = FFTLEN;

/* Collect bits until we have all of them */

	carry = 0;
	bitsout = 0;
	outptr = v->n;
	*outptr = 0;
	if (zp || generic)
		bits = ng / FFTLEN;

	for (i = 0; i < limit; i++) {
		val = (long) gg[i];
		if (!zp && !generic)
			bits = fftbase[i];
		val += carry;
		for (j = 0; j < bits; j++) {
			*outptr >>= 1;
			if (val & 1) *outptr += 0x8000;
			val >>= 1;
			bitsout++;
			if (bitsout == 16) {
				outptr++;
				bitsout = 0;
			}
		}
		carry = val;
	}

/* Finish outputting the last word and any carry data */

	while (bitsout || (carry != -1 && carry != 0)) {
		*outptr >>= 1;
		if (carry & 1) *outptr += 0x8000;
		carry >>= 1;
		bitsout++;
		if (bitsout == 16) {
			outptr++;
			bitsout = 0;
		}
	}

/* Set the length */

	v->sign = (long) (outptr - v->n);
	while (v->sign && (v->n[v->sign-1] == 0)) v->sign--;

/* If carry is -1, the gwnum is negative.  Ugh.  Flip the bits and sign. */
	
	if (carry == -1) {
		for (j = 0; j < v->sign; j++) v->n[j] = ~v->n[j];
		while (v->sign && (v->n[v->sign-1] == 0)) v->sign--;
		iaddg (1, v);
		v->sign = -v->sign;
	}

/* The gwnum is not guaranteed to be smaller than k*b^n+c.  Handle this */
/* possibility.  This also converts negative values to positive. */

	modg (gmodulus, v);

/* Since all gwnums are premultiplied by the inverse of k, we must now */
/* multiply by k to get the true result. */

	if (kg > 1) {
		giant	newg;
		newg = newgiant ((unsigned long) (bit_length / 16) + 3);
		itog ((int)kg, newg);
		mulg (v, newg);
		modg (gmodulus, newg);
		gtog (newg, v);
		gwpfree (newg);
	}

/* Return success */

	return (0);
}

char	timebuf[40];

void	gwpdone (void) {				// Free all the memory used by this code.
	printfunction = (verbose)? both_output : screen_output;
	MAXMULCONST = 1.0;
	gwpfree (fftbase);
	gwpfree (two_to_phi);
	gwpfree (two_to_minusphi);
	gwpfree (invlimit);
	gwpfree (flimit);
	gwpfree (hlimit);
	gwpfree (limitbv);
	gwpfree (cn);
	gwpfree (sn);
	gwpfree (permute);
	gwpfree (permutedx);
	gwpfree (permutedy);	
	gwpfree (ip);
	gwpfree (w);
	gwpfree (cnp);
	gwpfree (snp);
	if (zp)
		gwpfree (scr);
	gwpfree (GWP_RANDOM);
	gwpfree (modulus);
	gwpfree (recip);
	gwpfree (gwptmp);
	gwpfree (grecip);
	cuda_cleanup();
	if (debug) {
		if (MAXERR != 0.0) {
			sprintf (gwpbuf, "Maximum Round off : %10.10f\n", MAXERR);
			if (printfunction != NULL)
				(*printfunction)(gwpbuf);
		}
		gwpwrite_timer (timebuf, 5, TIMER_CLR);
		sprintf (gwpbuf, "Squaring and/or Mult. time : %s\n", timebuf); 
		if (printfunction != NULL)
			(*printfunction)(gwpbuf);
		gwpwrite_timer (timebuf, 2, TIMER_CLR);
		sprintf (gwpbuf, "Normalization time : %s\n", timebuf); 
		if (printfunction != NULL)
			(*printfunction)(gwpbuf);
		if (zp) {
			gwpwrite_timer (timebuf, 3, TIMER_CLR); 
			sprintf (gwpbuf, "Modular reduction time : %s\n", timebuf); 
			if (printfunction != NULL)
				(*printfunction)(gwpbuf);
		}
		if (generic) {
			gwpwrite_timer (timebuf, 4, TIMER_CLR); 
			sprintf (gwpbuf, "Generic copyzero + setzero time : %s\n", timebuf); 
			if (printfunction != NULL)
				(*printfunction)(gwpbuf);
		}
	}
	zp = 0;
	generic = 0;
}
