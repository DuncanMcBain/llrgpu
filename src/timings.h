#ifndef TIMINGS_H
#define TIMINGS_H

// Of course the guards are not currently useful
// but they might be later
#define NSQR_TIMER 10
#define TRNS_TIMER 11
#define NORM_TIMER 12
#define BSQR_TIMER 13
#define ASQR_TIMER 14
#define MAIN_TIMER 15
#define FFFT_TIMER 16
#define BFFT_TIMER 17
#define MULT_TIMER 18
#define INIT_TIMER 19

#endif
