\section{Porting to CUDA}

\subsection{GPU Architecture and Theory}

Throughout this discussion, ``host'' will refer to the CPU and main system
memory and ``device'' will refer to the GPU, its memory and associated hardware.

GPU development has largely been driven by the need for stronger hardware to
drive performance in games, with the architecture reflecting the strenuous
requirements of generating three dimensional scenes at ever-higher resolutions,
with higher triangle counts and larger textures, while still maintaining a sufficient
number of frames per second. In contrast to CPUs, which traditionally only have
a few hardware threads (though recently CPU design is heading for multi-core
chips) GPUs are massively parallel, with core counts running easily into the
hundreds. Games frequently require operations to be performed for every pixel on
the screen, for every triangle and similar operations, and the ``multiple
threads working on multiple data'' design reflects this. Originally there were
two main APIs which exposed this functionality (abstracting the hardware, so
that one piece of software could run on multiple devices): Direct3D and OpenGL.
Although powerful, these APIs were designed to assist with two and three
dimensional operations, and their use beyond this area was limited. However,
recent years have seen the rise of GPGPU - General Purpose computing on GPUs.
Through APIs like OpenCL and CUDA, programmers are now able to issue commands to
the hundereds of cores in GPUs and have them accelerate processes, potentially
leading to huge execution speedups. This project implemented a code in CUDA, so
this discussion of GPU architectures will focus on NVIDIA products. The concept
of multiple cores operating on multiple data streams is broadly applicable,
however there are some differences between the two vendors' hardware.

Figure \ref{fig:smdiagram} shows the structure of a Stream Multiprocessor in an
NVIDIA GF100 chip. The GF100 is not the SM used in the GTX 570, but its design
is useful for describing the operation of SMs in general.
Each SM consists of thirty-two CUDA cores, however, it can
be seen that there are far fewer scheduling units than cores. This design means
that every core within an SM must execute the same sequence of commands since
there are not enough schedulers to have multiple instruction streams. This is
the main way in which GPUs parallelise in order to speed up execution - by
applying the same operations to different data, which does put
some limits on the algorithms that can be ported to GPU platforms. Figure
\ref{fig:cudacore} shows the composition of each core; each is much more
simple than modern CPU cores. GPU cores typically have a much lower
clock rate than CPUs as well, but is through the number of cores
available that GPUs achieve their performance.

\begin{figure}
	\begin{center}
		\includegraphics[width=8cm]{smdiagram}
		\caption{A Streaming Multiprocessor found in the GF100 chip made by
		NVIDIA, adapted from\cite{ref:gf100}}
		\label{fig:smdiagram}
	\end{center}
\end{figure}

\begin{figure}
	\begin{center}
		\includegraphics[width=5cm]{CUDAcore}
		\caption{Close-up diagram of a CUDA core found in the GF100 SM, adapted
		from\cite{ref:gf100}}
		\label{fig:cudacore}
	\end{center}
\end{figure}

NVIDIA describes GPU computing as a ``grid of thread blocks'', where blocks map
to streaming multiprocessors and threads map to cores. A group of thirty-two
threads is called a ``warp''. Due to the low number of scheduling units, each
thread in a warp must execute the same instruction, which places a heavy penalty
on branching code. The grid of thread blocks abstraction is useful as it allows
the programmer to write code in a device-independent manner while maintaining
high performance. When executing code on the GPU, the programmer indicates the
number of threads to run in total by specifying the number of blocks per grid
(corresponding to the number of SMs) and the number of threads per block
(corresponding to the number of cores in an SM). While it would be possible to
hard-code these numbers for different models, this would be inefficient and
would require constant maintenance as new hardware is released. Instead, NVIDIA
recommend oversubscribing both the blocks per grid and the threads per block to
allow the thread scheduling engine to distribute the work across the available
hardware. This allows the programmer to avoid situations that might arise from
changes in more recent hardware, like an increase in core count in SMs. For
example, each SM in the GF100 chip has thirty-two cores, but previous SMs had
only eight. It took those units four times as many cycles to complete a warp since
they only had one quarter of the number of cores. If programmers had hardcoded
only eight threads per block, one quarter of the cores in GF100 chips would be
unused, a clear inefficiency.

Oversubscribing the work to be performed per block and per core has other
advantages, largely to do with memory access. GPU memory has been optimised for
bandwidth (for example, to move large textures in games) but suffers from high
latency. If there are sufficiently large number of threads, the scheduler can
`hide' the latency by swapping threads in and out of blocks when they make a
request for global memory accesses. It is also advantageous for the programmer
to ensure that the memory locations they access are adjacent, as the device can
then coalesce these memory accesses for a peformance increase.

As well as global memory, there are other smaller sections of faster memory
available. The programmer might choose to put constant but often-used data in
the `constant' memory section, or if there is mutable data to be shared among
cores in an SM, it might be stored in the `shared' address space.

Code declared as being a `kernel' is executed on the GPU. Each thread will
execute a copy of the function. It is common to have some large array and a task
that must be performed for each element in the array. When writing the kernel,
the programmer can ensure that each thread performs the correct unit of work by,
for example, calculating the thread ID then operating on the array element
indexed by that ID (this is the method primarily used in this project's CUDA
code). Code that has multiple branching statements is likely to perform poorly
on the GPU unless all cores within a warp take the same path of execution; if
they do not, every core will still perform the same actions but will discard the
data at the end, rather than saving the results. If all cores within a warp
branch in exactly the same way this is not a problem.

Kernel launches are asynchronous (and are executed synchronously on the GPU
unless the programmer specifies), allowing the programmer to perform other work
while the GPU executes the kernels. This is similar to the overlapping
communications and computation design pattern from, for example, MPI (Message
Passing Interface\cite{ref:mpi}). The programmer is able to launch a kernel and simultaneously
begin a memory transfer to or from the device, provided that the working area of
the kernel and the transfers do not overlap. This is hugely important, because
memory transfers between host and device are incredibly slow in either
direction. Although a large amount of bandwidth is available (less than memory
bandwidth, however), the latency is
quite high, and memory transfers can easily ruin the performance of a CUDA code.
It is best to avoid transferring data if at all possible;
performing additional computations on the host and device while transferring is
more efficient too.

In the last decade, there has been a large change in the design trends of CPUs.
Previously, each new CPU generation brought a speed increase largely through an
increase in clock rate (as well as a host of other important changes which are
tangential to this discussion). The key difference between GPUs and CPUs was the
massively parallel approach of GPUs, with a focus on simultaneous computation
compared the CPU's sequential operation. However, the last decade has seen a
halt in the increase of CPU clock speeds and a trend towards multiple cores in
CPUs as well. While there are still many fewer cores in a CPU than a GPU, to
obtain the best performance code must be prepared to take advantage of multiple
cores. CPUs are also capable of other simultaneous operations like performing
integer and floating point maths simultaneously as well as using Single
Instruction, Multiple Data (SIMD) extentions like SSE and AVX to operate on
multiple data elements at once.

In the future, designs might move towards increasing similarity between GPU and
CPU devices. Intel's Larrabee project aimed to create a sort of middle ground
between CPUs and GPUs. It was to support the x86 instruction sets like most CPUs
but was to have simpler cores (e.g. no support for out-of-order execution) like
GPU cores. In the end, the project was cancelled, but using it as a research
project Intel recently released the Xeon Phi. Each card has over sixty cores;
many more than any CPU but still far fewer than GPUs. It is capable of
hosting a simple operating system based on Linux. The Chinese supercomputer
Tianhe-2 has many of these devices installed as co-processors to its CPUs and
as of June 2013 is the fastest supercomputer in the world.

\subsection{The cuFFT Library}

To encourage programmers to attempt to harness the power of GPU computing both
hardware vendors provide highly-optimised mathematical libraries. Part of
NVIDIA's CUDA platform is the cuFFT library, a code designed to perform fast
Fourier transforms in both single and double precision in up to three
dimensions\cite{ref:cufft}. The interface was designed to be very similar
to FFTW's interface which is helpful in this project since LLRP uses FFTW for its
transforms, but is also useful for other developers, helping reduce the amount
that they have to learn when moving to a new platform.

cuFFT implements both the Cooley-Tukey and Bluestein algorithms for FFTs,
falling back to the latter when the FFT size is not decomposable into
sufficiently small prime factors. For this project, the FFT lengths are set such
that they have small prime factors. The code to set this length is due to
Shoichiro Yamada and is present in his code llrCUDA \cite{ref:llrcuda}. This means
that cuFFT is always able to use the more accurate Cooley-Tukey algorithm,
which has many optimised and hand-coded kernels in cuFFT.

\subsection{Porting in Stages}

It was decided that the porting attempt should consist of several stages: a
na\"{i}ve straight replacement attempt with no concern for memory transfers,
potential bottlenecks and so on; a version which attempted to keep the data on
the device between the two transforms, but copied it on and off either side of
those; and finally a version which only transferred data to the device at the
beginning of execution and transferred it off when execution was finished.This
was decided because each version provided a clear path to the next iteration of
the code, while still being relatively easy to implement. It also provided a
chance to catch regressions as they happened without having to roll back every
version of the code.

\lstset{language=C,
	basicstyle=\ttfamily,
	keywordstyle=\color{blue}\ttfamily,
	stringstyle=\color{red}\ttfamily,
	commentstyle=\color{red}\ttfamily,
	tabsize=4
}
\begin{figure}
	\begin{lstlisting}
// Copy onto the device, do the forward FFT, then copy off
cudaMemcpy(deviceArray, hostArray, size, cudaMemcpyHostToDevice);
cufftExecD2Z();
cudaMemcpy(hostArray, deviceArray, size, cudaMemcpyDeviceToHost);

// Executed on the host, not the device
_square_complex()

// Copy onto the device, do the backward FFT, then copy off
cudaMemcpy(deviceArray, hostArray, size, cudaMemcpyHostToDevice);
cufftExecZ2D();
cudaMemcpy(hostArray, deviceArray, size, cudaMemcpyDeviceToHost);
	\end{lstlisting}
	\label{fig:transfers}
	\caption{Pseudocode representing the order of memory transfers and
		Fourier transforms as in the first version of the code. Later
		versions removed the transfers either side of the squaring by
	implementing a CUDA kernel to perform the squaring.}
\end{figure}

Immediately it was seen that it would be extrememly easy to implement a
``drop-in'' replacement, given the similarity of the APIs of cuFFT and FFTW.
Therefore, code was written to create plans to execute the same transforms as
the FFTW version. However, the data required was not on the device, so a
synchronous data transfer was inserted before and after every transform. This
comes with a severe performance penalty: execution of the code could not
continue while the data was transferred, and such transfers are very slow.
They are often a bottleneck in CUDA codes. Four such transfers were
needed per iteration of the code; since the size of the data to be
transferred as well as the number of times the loop executes increases with
the size of the prime, the performance penalty was severe for larger primes.
Figure \ref{fig:transfers} shows the pattern of transfers and transforms.

\subsubsection{Eliminating the interior memory transfers}

To reduce the number of memory transfers, code to implement the
element-by-element squaring present between the two Fourier transforms was
chosen as the next target of the porting effort. This was the only operation
present between the forward and backward transforms so it was quite wasteful to
shuffle the data around so much for such a simple function. The code to square
the numbers was straightforward, however squaring on the device instead of the host
brought about a large improvement in performance simply by avoiding two
time-consuming memory transfers per iteration.

Figures \ref{fig:Csq} and \ref{fig:CUDAsq} show the similarity between the C code
and the CUDA code for squaring large complex numbers. The numbers are represented
as arrays of interleaved real and imaginary parts. The difference is that the
CUDA code is designed to run on a single element only. The manner in which all
elements of the array are transformed is to launch a kernel with as many threads
as there are array elements (due to the division of the threads into blocks, the
number of threads launched is actually equal to the smallest multiple of the
number of threads per block greater than or equal to the number of array
elements; this is why there is code to ensure that the thread index is not
greater than the array length). This pattern of parallelising loops with
independent iterations into CUDA kernels is a common idiom.

\lstset{language=C,
	basicstyle=\ttfamily,
	keywordstyle=\color{blue}\ttfamily,
	stringstyle=\color{red}\ttfamily,
	commentstyle=\color{green}\ttfamily,
	tabsize=4
}
\begin{figure}
	\begin{lstlisting}
void
_square_complex(
fftw_complex *b,
int n
)
{
	register int k;
	register double Reb;

	for (k=0; k<n; k++) {
		Reb = b[k][0]*b[k][0]-b[k][1]*b[k][1];
		b[k][1] = 2*b[k][1]*b[k][0];
		b[k][0] = Reb;
	}
}
	\end{lstlisting}
	\caption{Original C code to square an array of complex doubles element-by-element}
	\label{fig:Csq}
\end{figure}

\lstset{language=C++,
	basicstyle=\ttfamily,
	keywordstyle=\color{blue}\ttfamily,
	stringstyle=\color{red}\ttfamily,
	commentstyle=\color{green}\ttfamily,
	morekeywords={__global__},
	tabsize=4
}
\begin{figure}
	\begin{lstlisting}
__global__ void square_elem(cufftDoubleComplex *x, int length)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
	if(i >= length)
	return;
	double re = x[i].x * x[i].x - x[i].y * x[i].y;
	x[i].y = 2 * x[i].x * x[i].y;
	x[i].x = re;
}
	\end{lstlisting}
	\caption{CUDA code to square a single complex number out of an array; does
	nothing if its index is off the end of the array}
	\label{fig:CUDAsq}
\end{figure}

\subsubsection{Porting the remaining functions}

After the pairwise squaring was ported, only a small subset of the code remained
on the host. Two such sections were simply a weighting of the array elements,
necessary for the discrete weighted transform that this code implements.

The last remaining host function was inormalize.
In the process of `long' multiplication (the sort that is taught in schools) if
a digit in a particular position should become too large then a `carry' is
created; the carry is equal to the overflow from the lower digit divided by ten.
It is added on to the next digit in the sequence and the process repeats until
every digit in the number is less than the base the number is represented in
(most often ten). A similar operation must occur for the method of
multiplication implemented in this code. While the multiplication happens in
Fourier space, the normalisation, or carry-generating step is performed in real
space, and is analogous to the same step found in long multiplication.

There are other operations which must happen in this normalisation
code. The Fourier transforms are performed using double-precision numbers;
however, the numbers that are being tested are obviously integers. There is
some amount of rounding error associated with the transform in and out of
Fourier space, and as such the digits must all be rounded to the nearest integer
to ensure that the answer obtained is correct. In addition, LLRP adds the
constant value (+2) to the number at this stage of the computation. This does not
have to be done at this stage but it is convenient to do so. Figure
\ref{fig:normal} shows the first part of the normalisation procedure.

The digits are rounded by adding a sufficiently large value to the digit then
taking it away again. In arbitrary-precision arithmetic, this is a null
operation. However, floating-point mathematics is not associative, and when the
large value is added to the digit some trailing bits of data are `lost'. With
careful selection of the large value, the fractional part of the digit can be
removed, leaving an integral value (all integral values below $2^{53}$ can be
represented exactly in a double-precision float). This happens because the
processor shifts the numbers to be operated on such that they have the same
exponent. Values over a certain threshold only have enough precision to store
units, not tenths or smaller, and so the fractional part is removed. The reason
that the rounding is done this way is because it only takes two double-
precision additions; using a cast operation can take upwards of eighty cycles
in comparison! The technique of adding a large constant to the digits is
reused to create the carry value, since a larger number will `wipe out' even
more of the lower bits of the digit. Again, a careful choice of number will
ensure that the digit will remain less than the base the number is represented
in.

\lstset{language=C++,
	basicstyle=\ttfamily,
	keywordstyle=\color{blue}\ttfamily,
	stringstyle=\color{red}\ttfamily,
	commentstyle=\color{red}\ttfamily,
	morekeywords={__global__},
	tabsize=4
}
\begin{figure}
	\begin{lstlisting}
int j;
double * num;
double rounded, highBits, carry = 0.0;

for (j = 0; j < N; ++j)
{
	// Nearest-integer rounding
	rounded = (num[j] + bigVal) - bigVal;
	// Add the carry from previous iteration
	rounded += carry;
	// Removing the lower bits from the digit
	highBits = (rounded + limitbv[j]) - limitbv[j];
	// Compute the carry on next word
	carry = highBits * invlimit[j];
	// jth digit is now rounded and normalised
	num[j] = rounded - highBits;
}
	\end{lstlisting}
	\caption{Simplified code showing the sequence of operations in the
		inormalize function. It is not trivial to transform this loop into a
	CUDA kernel.}
	\label{fig:normal}
\end{figure}

One last pass must be made over the array in the case that the highest-position
digit has a carry associated with it. The way it is dealt with is that the carry
is rotated back to the lowest position of the array and, depending on the exact
way in which the number is to be treated, is added back in the lower digits. One
final normalisation pass is performed, since adding in any value might
unnormalise a digit of the number.

This code works well on a sequential device like a CPU, since each carry value
can potentially affect the digit above it. However, loops are most easily
parallelised in the CUDA paradigm when their iterations are independent - this
code is almost exactly the opposite. The only synchronisation available to CUDA
threads is between threads in the same warp. Across the entire grid, there is
none, save for atomic operations (which can be very slow if there is high
contention). As a consequence the CUDA normalisation code became very
complicated and difficult to implement. Several designs were attempted, which
often contained subtle data hazards where potentially two threads would be
attempting to access the same element at the same time, which CUDA provides no
protection against. Naturally, these erroneous versions did not work.

The end design settled upon having multiple separate kernels, since it is guaranteed
that all operations will have finished when the next kernel has been launched
(by using a synchronisation function). This comes with an associated performance
penalty as kernel launches are quite expensive. By using a global variable which
is updated using an atomic operation, the device is able to indicate to the host
how many carries remain, i.e. whether another pass needs to be performed on the
array to ensure that all digits are less than the base of the number. There is a
while loop which will, in order, add in the carries, set all of them save the
highest position to zero, renormalise then check whether any carries remain.
When no carries remain, the code leaves the loop and executes the
`propagate\_carry' kernel, which is the code that deals with the high-position
carry.

\lstset{language=C++,
	basicstyle=\ttfamily,
	keywordstyle=\color{blue}\ttfamily,
	stringstyle=\color{red}\ttfamily,
	commentstyle=\color{red}\ttfamily,
	morekeywords={__global__},
	tabsize=4
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% TODO TODO TODO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Do I keep this in? What you think?
\begin{figure}
	\begin{lstlisting}
int j;
double rounded, high_bits, carry2, carry = carries[len];
if (carry)
{
	j = 0;
	if(wrapindex)
		carry2 = carry * wrapfactor;
	carry *= -1;
	while (carry || carry2)
	{
		if (wrapindex && !carry)
			j = wrapindex;
		rounded = *(x + j) + carry;
		if (wrapindex && j == wrapindex)
		{
			rounded += carry2;
			carry2 = 0.0;
		}

		high_bits = (rounded + limitbv[j]) - limitbv[j];
		carry = high_bits * invlimit[j];
		*(x + j) = rounded - high_bits;

		if (++j == len)
		{
			j = 0;
			if (wrapindex)
				carry2 = carry * wrapfactor;
			carry *= -1;
		}
	}
}
	\end{lstlisting}
	\caption{Code that reconciles the carry values from the end of the array
		by adding the carry back onto the lower digits of the number. The translation
	from C to CUDA left it virtually unchanged.}
		\label{fig:normal2}
\end{figure}

This code does not produce the right answer for primes of greater than a certain size which is
disappointing, and the reason for its lack of correctness has not been
discovered. It is felt that the difficulty of implementing a loop with such
dependent iterations has led to a subtle error that prevents it from working as
intended.
