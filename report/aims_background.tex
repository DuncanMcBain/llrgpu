\section{Introduction}

{\it``Mathematicians have tried in vain to this day to discover some order in the
	sequence of prime numbers, and we have reason to believe that it is a mystery
into which the human mind will never penetrate.''} - Leonhard Euler \cite{ref:simmons}

Mathematicians have attempted to understand the manner in which the primes are
distributed throughout the integers, but have, as yet, been unable to do so. It
has been known since Euclid that the primes are infinite in number but the exact
sequence of primes is not predictable. The advent of computers, however, brought
with it a way to  investigate primes vastly larger than previous generations
were able to calculate, resulting in a search for ever-larger primes, so that we
might gain some insight into these strange mathematical beasts. This project
aims to accelerate primality testing in a code known as LLRP using GPUs.

\subsection{Motivation}

Prime numbers underpin the security of the Internet. When a shopper buys an item
on Amazon, their credit card details are protected by prime numbers. When
someone logs in to digital banking, thier details are kept secret because of the
sheer difficulty of factoring large integers quickly. However, it is not just
for security purposes that volunteers across the world search for prime numbers.
It is also for the sake of the knowledge itself, for the chance to participate
in a global search that pushes the boundaries of our computing technology and in
some cases advances our mathematical understanding. In January 2013, the largest
prime yet discovered was found as a part of the Great Internet Mersenne Prime
Search\cite{ref:48mersenne}. It is truly huge by everyday standards - it has 17,425,170 digits!
Where previously research departments in Universities performed the majority
of the computationally expensive
primality tests, nowadays the software is mostly run by volunteers as part of
distributed computing projects. Additionally the hardware which can
run such tests has diversified to include Graphics Processing Units as well as
Central Processing Units. In the last few years GPUs have increasingly been used
to accelerate numerical computation in games, consumer software and even the largest
supercomputers in the world \cite{ref:top500}. It is through Application Programming
Interfaces (APIs) like (CUDA and OpenCL) developed in the
last few years that the power offered by these cards has been made available for
non-graphical applications. Both  leading GPU manufacturers, NVIDIA and AMD,
provide high-performance drivers for their cards as well as specialised HPC
editions of their consumer-level cards. They also provide software libraries
for accelerating common mathematical codes, including linear algebra and fast
Fourier transforms (FFTs). The floating-point
capability of GPU hardware can vastly exceed that of CPUs, provided that the
software is able to fit the massively-threaded pattern required by the
architecture. While GPUs are not as versatile as
CPUs, they offer a huge amount of computation if the algorithm can be
structured to take advantage of it.

This project's aim is to port the numerical routines of LLRP (a freely available
program for primality testing) to the Compute
Unified Device Architecture (CUDA\cite{ref:cuda}), a platform for programming GPUs owned and
developed by NVIDIA.
The main computational task in the LLRP software is the execution of repeated
forward and backward FFTs. Therefore it makes sense to attempt
to accelerate certain numerical routines in LLRP by porting them to this platform
in order to speed up their execution. It would be possible to attempt
to write a new program from scratch, but that is likely infeasible. LLRP has
been developed over many years and to throw that accumulated knowledge and
effort away would be wasteful. Porting brings the advantages of a known-good
code and the speed of a graphics accelerator without requiring a total rewrite
and the effort that implies.

The decision to use CUDA, and particularly to use the cuFFT library, ties this
project to running on NVIDIA hardware, and so is contentious given that AMD also
has its own FFT library available\cite{ref:appml}. An alternative platform called
OpenCL (Open Computing Language \cite{ref:opencl}) has been standardised by the Khronos Group,
a consortium of companies with a common interest in promulgating open standards.
AMD is a part of this consortium and support the OpenCL standard on their GPUs.
OpenCL allows programmatic access to GPU hardware but is not locked to one
particular vendor, unlike NVIDIA's CUDA. One reason that CUDA was
chosen is that CUDA has been in production use for longer than OpenCL, which is
relatively new. Both NVIDIA and AMD have written mathematical libraries
optimised for their hardware so
this project would have had to target one library to the exclusion
of the other. It would have been possible to target OpenCL and write an
FFT implementation for this project specifically, to reduce fragmentation by
introducing a single version of the code, but since one aim is to achieve the
highest performance, using vendor-tuned libraries seems the most sensible
option. For these reasons, a CUDA port with cuFFT providing the fast
Fourier transforms was chosen.

Another project aim is to enable LLRP to make better use of the diverse
hardware available in modern desktop computers. This software is run largely
by volunteers interested in helping in the search for ever-larger primes, on many different
hardware configurations. Making use of all the hardware that these volunteers
have is desirable, particularly since using the GPU frees up CPU resources that
might otherwise interrupt the volunteers' use of their computing systems.
These volunteers run the software as part of large distributed computing
projects. There is an infinite number of primes and for that reason projects
seek to have highly efficient codes running on single cores, rather than some
difficult parallelisation of Fourier transforms across multiple cores.

\subsection{Project Proposal}

Prime numbers are of central importance to encryption systems worldwide,
yet the numbers themselves are interesting for a great many more reasons.
Determining the factors of a large integer is known to be computationally
very difficult, however there are methods which can check the primality of
large integers of certain forms efficiently. There are a few such classes of
number -
Generalized Fermat Primes $(b^{2^{n}} + 1)$, Proth and Riesel numbers
$(k \times 2^n \pm 1)$ and Mersenne Primes $(2^n-1)$. Just this year the 48th
Mersenne prime was found (taking 39 days on a CPU), with a verification run
being performed in only 4 days using a GPU. It has over 17 million digits
and is currently the largest known prime. However, it is not known whether
there are finitely many Mersenne primes or not.

Codes exist to test all these types of number and the tests are suited to
optimisation by parallelisation on a GPU. Highly-optimised CPU code exists
but is sometimes not portable, which is key as these projects are used in
distributed computing projects like GIMPS and PrimeGrid. Therefore, efficient
use of desktop machines is desirable.

The aim of this project is to port the plain-C LLRP code to the CUDA
platform, with the aim of maximising the amount of work performed on the
GPU and minimising the CPU usage. This will not only increase the
efficiency of the code by minimising time spent transferring data between
the CPU and GPU - often the slowest step - but will also allow the code to
be run on desktop machines that have capable GPUs with minimal impact on
the CPU. Performance comparisons between LLR, the new code and the existing
LLRCUDA port will be made.

\subsection{Background}

Software that can test the primality of numbers is used in a number of online
distributed computing projects, including the Great Internet Mersenne Prime
Search\cite{ref:gimps} (GIMPS) and PrimeGrid\cite{ref:primeg}. These projects
attempt to discover prime numbers not only for the sake of curiosity but also
to satisfy certain hypotheses that, while lacking a mathematical proof, make
some claim that can be searched exhaustively. The ``Seventeen or bust''
project\cite{ref:17orb} is an example of a group attempting to prove a hypothesis
by exhaustive search. This project is attempting to solve the Sierpinski
problem - to find the smallest Sierpinski number. A number $k$ is a Sierpinski
number if $k2^N + 1$ is not prime for all $N$; in 1967 Sierpi\'nski and
Selfridge conjectured that 78,557 was the smallest such number. Seventeen or
Bust is searching for primes of the form $k2^N + 1$; there are very few $ks$
left that are smaller than 78,557.

The software LLR was developed to test the primality of large integers. It
utilises various algorithms to determine the primality of many different types
of number. Numbers of the form $N = k \times 2 ^ n - 1$ can be tested by the
Lucas-Lehmer-Riesel algorithm\cite{ref:riesel} which is the code path this
project aims to accelerate.

\begin{figure}
	\begin{centering}
	\includegraphics[width=8cm]{Flowchart}
	\label{fig:flowchart}
	\caption{A flowchart showing the core of the Lucas-Lehmer-Riesel algorithm.
		The process loops $n - 2$ times, at which point the program checks if
	$u_{n - 2} / N = 0$; if it does, the number is prime.}
\end{centering}
\end{figure}

The algorithm is an extension of the Lucas-Lehmer test used for Mersenne numbers and as such
has similar structure. In short, given some starting value $u_0$ (which is
determined by the constants that comprise $N$), the sequence $\{u_i\}$ is
defined as $u_i = u_{i - 1}^2 - 2$. If $N$ divides $u_{n - 2}$, then $N$
is a prime number; otherwise, it is not. It is worth noting that in the case
that $N$ is a composite number none of its prime factors are revealed.
Factorisation is more computationally demanding and as such these
efficient prime tests do not provide a `short cut' to obtain prime factors more
quickly than integer factorisation is able to provide.

It can therefore be seen that the core of the algorithm is calculation of the
sequence $\{u_i\}$. Normally integer multiplication is extremely fast on modern
CPUs and is not a performance issue; however, the numbers being tested by this
algorithm go far beyond the limits of even 64-bit floats. Na\"{\i}ve, or `long'
multiplication, has a time complexity of  $O(n^2)$ (for multiplying two numbers
of length $n$) which would take far too long to execute. Therefore, a faster
multiplication method is required.

The method implemented in this code is due to Schonhage and
Strassen\cite{ref:sands} (see also \cite{ref:candf}). The numbers
are represented by floating-point arrays where each array element represents one
`digit' of the number (not necessarily in base ten, but this is an implementation
detail and does not affect the description of the ideas of the algorithm). To
obtain the product of the two numbers, they are first Fourier transformed, then
each matching `digit' is multiplied together. When the reverse transform is
performed, the result will be the product of the two numbers. An additional
normalisation step must be performed in order to ensure that each `digit' is
less than the base (analogous to the carry generated when multiplying in base
ten, using long multiplication).

The complexity of the fast Fourier transform is $O(n \log(n))$ for a transform
of length $n$; the piecewise multiplication has only $n$ complex multiplications
and so is essentially negligible. for sufficiently large $n$. The size of
transforms performed in LLR is typically in the tens of thousands but can be
much larger. It can be seen that this is an algorithmic
speedup over na\"{\i}ve multiplication and is well worth the effort of
implementing. This means that the most expensive operations in the main loop are
now the forward and reverse transforms.

\subsection{Related Software}

The distributed computing projects, the GIMPS and PrimeGrid, make use of many
different numerical codes to test their primes, depending on the class of
number to be tested. These include Prime95\cite{ref:prime95} and Genefer
\cite{ref:genefer} as two of the more common codes; however the ones of
particular interest to this project are LLR, an implementation of the Lucas-
Lehmer-Riesel algorithm using the gwnum library; LLRP, a plain-C version of
the previous code and llrCUDA, a CUDA port of LLRP written by Shoichito Yamada.

\subsection{Hardware}

This project's test machine consists of an Intel Sandy Bridge 2500k with an
NVIDIA GTX 570 running Arch Linux. It was developed targeting CUDA version 5.
Since the majority of the computers running this software have consumer
hardware rather than the High Performance Computing editions of this card,
developing it on consumer hardware more accurately reflects the settings in
which the software will be used.

