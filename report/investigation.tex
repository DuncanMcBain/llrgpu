\section{Investigation of LLRP}

\subsection{LLRP Overview}

LLR is a code developed by Jean Penn\'e, utilising the gwnum library written by
George Woltman, that implements the Lucas-Lehmer-Riesel algorithm. It is
capable of testing the primality of many more types of number than
Riesel primes using a variety of techniques, though these are of secondary
importance to this project. The original gwnum library
uses hand-coded assembly to perform its FFTs allowing it to do so very quickly -
for example, the most recent versions are capable of taking advantage of AVX
\cite{ref:avx} instructions present in modern processors from AMD and Intel.
However, when no assembly has been written for an architecture, the code simply will
not run.

Penn\'e has also written a version of the gwnum library which does not use the
assembler present in the original but is instead designed to be portable to any
system with a C compiler. This rewrite is called gwpnum and is embedded in the
LLRP application. While it is slower, is a suitable starting point for a port
to CUDA for two main reasons. Firstly, its portability; secondly, the interfaces
to the cuFFT library are very similar to the API of the library known as ``The
Fastest Fourier Transform in the West'' (FFTW \cite{ref:fftw})
which is used for the transforms in LLRP. It will run virtually anywhere but
is considerably slower for this specific program.

The main interface to the program is through the command line, where users are able
to describe a number to the program using mathematical operators like *, \^{} and
so on. An example invokation of the program might look like \verb+./llrp -d -q"11*2^74726-1"+.
This method of specifying the task allows it to be a part of
distributed online computing projects like PrimeGrid such that the controlling
software can distribute tasks to a number of volunteer machines, run these
tasks and collect the output without user intervention. The program's output
is in a standard format allowing it to be interpreted by automated scripts.

The program is split into several different object code files. The functions
that gwnum provides are concatenated into a statically-linked archive allowing
the main executable access to all the functionality of gwnum. This design is
followed in LLRP since Penne re-uses the LLR-specific code and links gwpnum
instead of Woltman's gwnum code. Therefore, the only difference between LLR and LLRP is
in the numerical library, renamed gwpnum in LLRP. This modularity ensures that
the two versions of the library are completely interchangeable removing the need
to maintain two separate implementations of the Lucas-Lehmer-Riesel algorithm,
and so on. One object contains the implementation of the Lucas-Lehmer-Riesel
algorithm and organises the code to execute based on the number that it is to
test (for example, the Lucas-Lehmer test for Mersenne primes, the Proth test and
so on). The other objects contain the main startup code as well as utility
functions for file handling, memory management and other important but, for this
project, inconsequential functionality.

\subsection{Data Flow}

\begin{figure}[hp]
	\begin{center}
		\setlength\fboxsep{0pt}
		\setlength\fboxrule{0.5pt}
		\fbox{\includegraphics{callgraph}}
		\caption{A graphical representation of the important functions called when
		running the Lucas-Lehmer-Riesel algorithm in LLRP}
		\label{fig:calltree}
	\end{center}
\end{figure}

Tracking data through the Lucas-Lehmer-Riesel execution path provided a great
deal of insight into the operation of the program. Figure \ref{fig:calltree}
shows the branch of the call tree which leads to the actual squaring of the
large potential prime. There are other functions executed along other branches
of the program but they are less relevant to this analysis of the data flow, as
they will not be affected by the CUDA porting efforts. The main entry point to the
program processes any command-line arguments which might have been passed to the
program and then continues execution based on those arguments. The program
ensures that no other copy is running (\texttt{LinuxContinue()}) then classifies
the number according to the form it takes (say, $k \times b^n \pm c$) in
\texttt{PrimeContinue()}. This chains into \texttt{process\_num()} which will
then call the appropriate \texttt{is*()} function, e.g. \texttt{isLLRP()},
depending on which algorithm is to be executed as decided by the form of the
number. For this project in particular, the function of interest is
\texttt{isLLRP()}, which implements the Lucas-Lehmer-Riesel algorithm.

Any code deeper in the tree than this is now executed in the gwpnum library.
\texttt{isLLRP()}, after initialising several key variables in the gwpnum
library, initiates the main loop of the program. It calls the function
\texttt{gwpsquare()} whose main function is to call
the lower-level \texttt{lucas\_square}. The purpose of \texttt{lucas\_square} is
to perform many of the lower-level operations necessary before the actual
Fourier transforms can be executed and then to normalise the output received from
\texttt{fftwsquare\_g()}. Optionally, some error checking is performed.

In \texttt{fftwsquare\_g()}x, the main computation occurs. As can be seen in
figure \ref{fig:calltree}, the three most important functions are
\texttt{fftw\_execute} (called twice) and \texttt{\_square\_complex()}. This is
where the actual squaring of the number happens, bounded by the forward and
backward Fourier transforms.

Analysis of this program revealed that for the Lucas-Lehmer-Riesel algorithm,
none of the boolean values \texttt{zp}, \texttt{generic} or \texttt{compl} are
true. Therefore the sections of code executed when those variables are true have
been excluded from this analysis. The lower-level functions of gwpnum are
more generic than required for just the LLR algorithm and as such have
alternative functionality controlled by these flags; for example, \texttt{zp}
controls whether the FFTs are zero-padded, \texttt{generic} is used when the
expression has an added component that is not $\pm 1$ and \texttt{compl}
controls transforming a real FFT of length $N$ into a complext FFT of length
$N/2$.

\subsection{Results of Profiling}

It is possible to compile programs on GNU/Linux with extra instrumentation code
(using the compile-time flag -pg)
that will profile the program at runtime, allowing the programmer to obtain
useful information about how the code performs. The gprof tool can create a flat profile
showing how often functions were called and for how long they were running, a
useful tool to see where the code spends the most time. This can help the
programmer's optimisation efforts ensuring that they do not waste time
optimising slow code that is only executed a handful of times, for example.
Additionally the instrumentation is capable of generating a call graph similar
to the hand-produced graph in figure \ref{fig:calltree}, albeit one that is much
more complex and comprehensive. Significant portions of a flat profile for LLRP, testing the primality
of the prime number $11\times 2^{74726} - 1$, are reproduced in figure
\ref{fig:flatprof}. However, it is worth noting that this profiler output is not
exactly accurate for this code because it has not instrumented large parts of
the FFTW code which are compiled in a separate listing.
This is most apparent in that the running time is quoted as a
little over five and a half seconds in the profiler but the actual time elapsed
was a little over eleven seconds. The missing time is the time spent in the FFTW
library.

\begin{figure}
	\footnotesize\begin{verbatim}
	Flat profile:

	Each sample counts as 0.01 seconds.
	  %   cumulative   self              self     total
	time   seconds   seconds    calls   s/call   s/call  name
	43.02      2.31     2.31    74730     0.00     0.00  inormalize
	30.54      3.95     1.64    74727     0.00     0.00  lucas_square
	22.91      5.18     1.23    74727     0.00     0.00  _square_complex
	 0.56      5.21     0.03       35     0.00     0.00  fftinv_hermitian_to_real
	 0.37      5.23     0.02  1123531     0.00     0.00  dd_real::dd_real
	 0.37      5.25     0.02   157743     0.00     0.00  operator/
	 0.37      5.27     0.02      689     0.00     0.00  check_balanced
	 0.19      5.28     0.01  1620394     0.00     0.00  quick_two_sum
	 0.19      5.30     0.01   358493     0.00     0.00  two_diff
	 0.19      5.31     0.01   329821     0.00     0.00  operator*
	 0.19      5.32     0.01    14337     0.00     0.00  exp
	 0.19      5.33     0.01    14335     0.00     0.00  nint
	 0.19      5.34     0.01       53     0.00     0.00  fft_real_to_hermitian
	 0.19      5.35     0.01       35     0.00     0.00  addsignal
	 0.19      5.36     0.01        3     0.00     0.00  lucas_mul
	 0.19      5.37     0.01        1     0.01     5.37  isLLRP
	\end{verbatim}
	\label{fig:flatprof}
	\caption{Truncated output of the gprof profiling tool after instrumenting
		the program LLRP.}
\end{figure}

It can be seen that the functions \texttt{inormalize}, \texttt{lucas\_square}
and \texttt{\_square\_complex} take the most time to execute (not including the
missing Fourier transform time). This is a consequence of the core of the
algorithm: the squaring happens $n - 2$ times; in this case, 74724 times. Some
other constants must be determined before the algorithm can be executed and
these are determined using a similar process to the algorithm's core. This
explains the slightly higher execution count for the three functions.

The call tree, together with the profiler output, demonstrates what the key
numerical routines at the heart of the Lucas-Lehmer-Riesel test section of the
LLRP code are and therefore what the focus of this project should be.

\subsection{Deciding Project Scope}

The call tree shows a strong divide between the high-level algorithmic code
and the lower-level mathematical code in LLRP. Similarly, most of the time spent
in execution is in these lower-level mathematical functions. This indicates that
the main efforts should be in the gwpnum library, accelerating the transforms
and associated code, but maintaining encapsulation of the CUDA implementation
below the point that the code in the algorithmic section might see.

It would be possible to alter more of the lower-level code such that other code
paths (like, for example, the Proth primality test) might be accelerated too;
however, it is felt that accelerating the code in the Lucas-Lehmer-Riesel test
is sufficient for a project of this length.
