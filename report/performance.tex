\section{Performance Analysis}

\subsection{Comparison of Times for Various Primes}

The code has many timing functions available for both debug and informational
purposes. To have a more fine-grained profile of how long the program spent
executing certain sections of the code, timing functions were inserted at key
points in the program's squaring code. These were used to track how long the
data transfers ran for, or how quickly the transforms were executed. These times
are stored cumulatively for the entire running time of the program.
Figures \ref{fig:pie1} to \ref{fig:pie4} shows the percentage time spent in
key areas of the program for select primes. The timers cover the time spent
executing the transforms, the time spent normalising, the time spent
transferring data, the complex squaring, the weighting of the number before and
after the transforms and the time taken to initialise the system. Any
discrepancy between the total time recorded by these timers and a timer that
covers the execution of the program is represented as `other'.

The primes were chosen to cover a range of sizes while staying small enough to
be tested in a matter of seconds to ensure rapid testing when developing the
code. Finally a very large prime was chosen to investigate the behaviour of the
program at large N. The trend is that
proportionally less time is spent executing the FFTs compared to the
normalisation, which still runs on the CPU, and time spent waiting for memory
transfers to complete, which are necessary for the normalisation to run. For
example, in figure \ref{fig:pie2} the FFTs take approximately 20\% of the
running time, dropping to a little over 10\% of the running time in figure
\ref{fig:pie4}.

\begin{figure}
	\begin{center}
	\begin{tikzpicture}
		\pie[text=legend]{ 19.5/gwpinit, 10.3/Pairwise multiplication,
			17.6/Forward FFT, 17.9/Backward FFT, 6.7/Forward weighting,
		6.7/Backward weighting, 20.3/Normalisation, 1.1/Other}
	\end{tikzpicture}
	\caption{The percent time spent in each section of code in the original
	LLRP code}
	\label{fig:pie1}
\end{center}
\end{figure}

\begin{figure}
	\begin{center}
	\begin{tikzpicture}
		\pie[text=legend]{ 9.7/gwpinit, 19.3/Pairwise multiplication,
			8.6/Forward FFT, 7.8/Backward FFT, 7.7/Forward weighting,
			18/Backward weighting, 11.9/Normalisation, 16/Memory transfers,
			0.9/Other}
	\end{tikzpicture}
	\caption{llrgpu timings for $11*2^{74726} - 1$}
	\label{fig:pie2}
\end{center}
\end{figure}

\begin{figure}
	\begin{center}
	\begin{tikzpicture}
		\pie{ 17.7/, 10.4/,
			5.5/, 4.9/, 8.5/,
			10.4/, 18/, 18.7/,
			6/}
	\end{tikzpicture}
	\caption{llrgpu timings for $13*2^{166303} - 1$}
	\label{fig:pie3}
\end{center}
\end{figure}

\begin{figure}
	\begin{center}
		\begin{tikzpicture}
			\pie{ 9.4/, 10.1/,
				5.7/, 5.5/, 10/,
				10.7/, 21.8/, 21.5/,
			5.4/}
		\end{tikzpicture}
		\caption{llrgpu timings for $13*2^{233207} - 1$}
		\label{fig:pie4}
	\end{center}
\end{figure}

Figure \ref{fig:pie1} shows the time spent inside the plain LLRP code. The next
three show the same code sections but in the final version of the llrgpu code,
with FFTs and so on performed on the GPU. It can be seen that the FFTs take
proportionally less time in the GPU accelerated code, indicating that the GPU
code is faster. This aspect of the code could be considered highly successful.

The size of the prime being tested increases across the three charts. The charts
indicate that the proportion of time spent transferring data and normalising the
number is now the bottleneck. If a suitably fast GPU normalisation routine were
written, it would solve both problems at once; however, implementing such a
function correctly and quickly remains very difficult.

\begin{figure}
	\begin{center}
	\begin{tikzpicture}
		\pie[text=legend]{ 3.6/gwpinit, 9.8/Pairwise multiplication,
			1/Forward FFT, 0.7/Backward FFT, 10.9/Forward weighting,
			10.4/Backward weighting, 36.8/Normalisation, 26.6/Memory transfers,
			0.2/Other}
	\end{tikzpicture}
	\caption{llrgpu timings for $3*2^{3136255} - 1$}
	\label{fig:chartbig}
\end{center}
\end{figure}

To test how the code could deal with very large primes, the prime number $3
\times 2 ^{3136255} - 1$ was tested. The total running time was 6670 seconds,
including time taken for the library to initialise; figure \ref{fig:chartbig}
shows the breakdown of times. Overwhelmingly the most time was taken in
normalising the number. The transforms took less than 2\% of the running time
of the program. It is a testament to the power available in GPUs that the
worse-scaling section of the code should execute so much more quickly than the
essentially linear normalisation. Of course, the memory transfers also take up a
large amount of time and it would be a strong optimisation to be able to remove
them from the code. 

In addition to timing different sections of the same code, comparisons were made
between the new GPU code and the original LLR code which
is highly optimised to run on CPUs. It was also compared against the existing
CUDA port known as llrCUDA; the results are in table \ref{primetimes}.

\begin{table}
	\begin{center}
		\begin{tabular}{||r|c|c|c|c||}
			\hline
			Primes & $11 \times 2^{74726} - 1$ & $13 \times 2^{166303} - 1$ & $13 \times 2^{233207} -1$ & $17 \times 2^{2946584} - 1$ \\
			\hline
			{\bf llr} & 1.260 s & 6.795 s & 11.762 s & 2514.362 s\\
			\hline
			{\bf llrCUDA} & 35.336 s & 81.192 s & 112.283 s & * \\
			\hline
			{\bf llrGPU} & 9.552 s & 33.686 s & 50.896 s & 6857.843s\\
			\hline
		\end{tabular}
		\caption{A table of primality test timings. Each code was tested with
		several primes in order to compare their relative performance. llrCUDA
	crashed during the testing of the largest prime.}
		\label{primetimes}
	\end{center}
\end{table}

The table indicates that LLR (using the hand-optimised functions of gwnum)
is still the fastest implementation of the
Lucas-Lehmer-Riesel algorithm. Nevertheless, an improvement over the current
CUDA port has been obtained, which is very promising. If the GPU's power could
be leveraged for the normalisation routines, not only would the memory transfers
be eliminated, reducing the run time considerably, it might even prove faster
than current CPU code, making it exceedingly competitive with LLR.

During development the code transitioned through many different versions, as
features were developed and refined, but it is interesting to investigate the
effects of memory transfers on performance. Table \ref{tab:dev_vers} shows how
the first in-development version of the code performed. In the first, the data
was only moved onto the device when a transform was required and was taken off the
device immediately after. This resulted in a heavy load of four memory transfers
per iteration and performance suffered as a result.

The version of the code featuring a CUDA normalisation function enters an
infinite loop making it impossible to time.

\begin{table}
	\begin{center}
		\begin{tabular}{||r|c|c|c||}
			\hline
			Primes & $11 \times 2^{74726} - 1$ & $13 \times 2^{166303} - 1$ & $13 \times 2^{233207} -1$ \\
			\hline
			llrgpu & $13.948$ s & $44.388$ s & $74.007$ s \\
			\hline
		\end{tabular}
		\label{tab:dev_vers}
	\end{center}
	\caption{A table showing results for an in-development version of the
		code; the code is significantly slower due to the additional memory
	transfers that are required each iteration.}
\end{table}

\subsection{NVIDIA Profiling Tools}

The CUDA toolkit comes with many additional tools as well as libraries. One such
tool is the NVIDIA profiling tool, a program that is the GPU equivalent of
gprof. The profiler tool will measure how long the graphics card device spends
executing each function and can even suggest areas for performance improvement,
by indicating whether a kernel is slowed down by memory latency, memory
bandwidth and so on. In this project it was useful in proving that one of the
major sources of bad performance was the time spent in device driver code
waiting for memory transfers to complete, even though they took less time to
execute on the device. It also showed that for the very simplest of the kernels
being executed by the device, the main barrier to better performance was memory
latency, since often kernels only required a few bytes of data from main memory.
As a result, kernels were often waiting on memory accesses to complete before
continuing execution.

It is worth noting that the profiler tools do have some associated overhead,
though the tools try to account for time spent acquiring profile data in the
output. However, the slowdown can make running tests on large data sets
infeasible as the program takes longer and longer to run with the profiling
enabled.

\begin{figure}
	\scriptsize\begin{verbatim}
==4457== Profiling application: ./llrgpu -d -q11*2^74726-1
==4457== Profiling result:
Time(%)      Time  Calls       Avg       Min       Max  Name
 32.79%  1.50685s  74690  20.174us  19.780us  20.924us  void dpVector2048D::
                                                        kernelTex<fftDirection_t=-1>
 32.59%  1.49766s  74690  20.051us  19.787us  20.795us  void dpVector2048D::
                                                        kernelTex<fftDirection_t=1>
  9.67%  444.31ms  74725  5.9450us  5.8870us  17.151us  [CUDA memcpy DtoH]
  7.59%  348.65ms  74729  4.6650us  4.4150us  11.936us  [CUDA memcpy HtoD]
  6.35%  292.04ms  74724  3.9080us  3.5150us  4.8020us  void dpRealComplex::
                                                        preprocessC2C_kernelMem
  5.35%  245.67ms  74724  3.2870us  2.7340us  6.5590us  void dpRealComplex::
                                                        postprocessC2C_kernelMem
  3.80%  174.75ms 149448  1.1690us     893ns  2.0120us  convolution_elem
  1.87%  85.802ms  74724  1.1480us     925ns  1.6960us  square_elem
  \end{verbatim}
  \label{fig:nvprof}
  \caption{Sample output from the command-line profiler. It shows that on the
	  device, the transforms take the most computation. The average, minimum and
	  maximum times are useful for showing that sometimes memory transfers can
  take much longer than the average (in this example, 17 microseconds, over
  three times as long as the average.}
\end{figure}

Figure \ref{fig:nvprof} shows the output of the profiler (function arguments
have been removed for greater clarity). It is useful to see where the device
spends its time as it is difficult for the programmer to interact directly with
the GPU device.

There is a GUI version of the profiler called the NVIDIA visual profiler. It
attempts to show similar information in a more intuitive way with bars of colour
representing what computation is taking place at any given interval. The amount
of data that can be collected by the tools is huge and can help the programmer
completely understand the reasons behind the performance of his or her code.
